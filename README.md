<pre>
   ______ _____   ___      __  ___   
  / ____// ___/  /   |    /  |/  /   
 / / __  \__ \  / /| |   / /|_/ /    
/ /_/ / ___/ / / ___ |_ / /  / /     
\____(_)____(_)_/  |_(_)_/  /_(_)    
</pre>
A tool that performs Genome Scale Atom Mapping. The results can be exploited to build carbon skeleton networks for topological analysis of genome-scale metabolic networks (GSMN), and analyse atom tracking results.
GSAM is a built around the Reaction Decoder Toolkit, a library which compute atom mappings from reactions represented as RxnSmiles.
It uses the JSBML library to import a GSMN in standard format, then automatically generate all the RxnSmiles from provided compounds structures, launch the atom mapping and finally parse the results to render meaningful substrate-products transitions.
Since many reactions in SBML are not suited for AAM (such as biomass production), GSAM provides flexible filtering utilities and extensive logs for adapting a GSMN to AAM tasks.

GSAM requires a metabolic network in SBML (Systems Biology Markup Language) file format and a table containing SMILES (Simplified molecular-input line-entry system) representing the chemical structure of the compounds in the model
Find more about the network format at http://sbml.org and the structure representation at https://www.daylight.com/smiles/index.html
SBML networks can be retrieved, for example, from MetExplore (http://www.metexplore.fr/).

Usage:

```
	java -jar GSAM.jar -i path/to/sbml.xml -s path/to/smiles.tab -o path/to/outputDir

 -a (--atom) VAL        : filter option: atom tracked (default: C)
 -c (--count)           : output option: write number of mapped atoms rather
                          than whole map (default: false)
 -col1 (--idcolumn) N   : SMILES import option: column containing GSMN
                          compounds ids (default: 1)
 -col2 (--smicolumn) N  : SMILES import option: column containing SMILES
                          (default: 2)
 -h (--help)            : prints the help (default: true)
 -i (--sbml) VAL        : input SBML file
 -m (--max) N           : filter option: max number of atoms in reaction
                          (default: 1000)
 -o (--outdir) VAL      : path to the output directory
 -s (--smi) VAL         : input SMILES table
 -sep (--separator) VAL : SMILES import option: character delimiting columns
                          (default: 	)
 -sh (--skpiheader)     : SMILES import option: table contains header (default:
                          false)
```	

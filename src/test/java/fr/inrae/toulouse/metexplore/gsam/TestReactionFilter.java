package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.utils.Flag;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.utils.FilterBuilder;
import fr.inrae.toulouse.metexplore.gsam.utils.ReactionFilter;
import org.junit.jupiter.api.*;
import org.openscience.cdk.exception.CDKException;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;

import java.util.EnumSet;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for simple AAMSmilesComputor.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestReactionFilter
{
    public GSAMReaction r1,r2,r3,r4;

    /**
     * create all reactions to filter
     */
    @BeforeAll
    public void init(){
        try {
            /* Species as = new Species("a");
            Species bs = new Species("b");
            Species cs = new Species("c");
            Species ds = new Species("d");*/

            System.out.println("build compound");
            GSAMCompound a = new GSAMCompound(new Species("a"),"OP(O)(=O)OC(=C)C([O-])=O");
            //System.out.println(a.getCDKmolecule().getAtomCount());
            GSAMCompound b = new GSAMCompound(new Species("b"),"[H]O[H]");
            //System.out.println(b.getCDKmolecule().getAtomCount());
            GSAMCompound c = new GSAMCompound(new Species("c"),"OC[C@@H](OP(O)(O)=O)C(O)=O");
            //System.out.println(c.getCDKmolecule().getAtomCount());
            GSAMCompound d = new GSAMCompound(new Species("d"),"*");
            //System.out.println(d.getCDKmolecule().getAtomCount());

            //balanced, has carbons, 10(+5H)+1(+2H)->11(+7H) total 22(+14H) atoms, 3 participants
            HashMap<GSAMCompound,Integer> sr1 = new HashMap<>();
            HashMap<GSAMCompound,Integer> pr1 = new HashMap<>();
            sr1.put(a,1);
            sr1.put(b,1);
            pr1.put(c,1);
            r1 = new GSAMReaction(new Reaction("r1"),sr1,pr1);

            //not balanced, has carbons, >1500 atoms, 100 participants
            HashMap<GSAMCompound,Integer> sr2 = new HashMap<>();
            HashMap<GSAMCompound,Integer> pr2 = new HashMap<>();
            sr2.put(a,99);
            pr2.put(c,1);
            r2 = new GSAMReaction(new Reaction("r2"),sr2,pr2);

            //not balanced, no carbon in products, 10(+5H)+10(+5H)+1(+2H)->1* total 22(+12H) atoms, 4 participants
            HashMap<GSAMCompound,Integer> sr3 = new HashMap<>();
            HashMap<GSAMCompound,Integer> pr3 = new HashMap<>();
            sr3.put(a,2);
            sr3.put(b,1);
            pr3.put(d,1);
            r3 = new GSAMReaction(new Reaction("r3"),sr3,pr3);

            //balanced*, no carbon in products nor substrates, 1*->1* total 2 atoms, 2 participants
            HashMap<GSAMCompound,Integer> sr4 = new HashMap<>();
            HashMap<GSAMCompound,Integer> pr4 = new HashMap<>();
            sr4.put(d,1);
            pr4.put(d,1);
            r4 = new GSAMReaction(new Reaction("r4"),sr4,pr4);

        } catch (CDKException e) {
            e.printStackTrace();

        }
    }

    /**
     * reset flags
     */
    @AfterEach
    public void clean(){
        r1.setFlags(EnumSet.noneOf(Flag.class));
        r2.setFlags(EnumSet.noneOf(Flag.class));
        r3.setFlags(EnumSet.noneOf(Flag.class));
        r4.setFlags(EnumSet.noneOf(Flag.class));
    }

    /**
     * Test filtering based on atoms count
     */
    @Test
    public void testAtomCountFilter()
    {
        ReactionFilter f = new FilterBuilder().maxCountOfTotalAtoms(25).build();

        f.checkAndFlag(r1);
        assertFalse( r1.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertTrue( f.keep(r1) );
        assertTrue( f.keepUnchecked(r1) );

        f.checkAndFlag(r3);
        assertFalse( r3.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertTrue( f.keep(r3) );
        assertTrue( f.keepUnchecked(r3) );

        f.checkAndFlag(r4);
        assertFalse( r4.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertTrue( f.keep(r4) );
        assertTrue( f.keepUnchecked(r4) );

        //-------------------------------

        f.checkAndFlag(r2);
        assertTrue( r2.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertFalse( f.keep(r2) );
        assertFalse( f.keepUnchecked(r2) );

    }

    /**
     * Test filtering based on number of participant
     */
    @Test
    public void testParticipantCountFilter()
    {
        ReactionFilter f = new FilterBuilder().maxCountOfParticipants(3).build();

        f.checkAndFlag(r1);
        assertFalse( r1.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertTrue( f.keep(r1) );
        assertTrue( f.keepUnchecked(r1) );

        f.checkAndFlag(r4);
        assertFalse( r4.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertTrue( f.keep(r4) );
        assertTrue( f.keepUnchecked(r4) );
        //-------------------------------

        f.checkAndFlag(r2);
        assertTrue( r2.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertFalse( f.keep(r2) );
        assertFalse( f.keepUnchecked(r2) );


        f.checkAndFlag(r3);
        assertTrue( r3.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertFalse( f.keep(r3) );
        assertFalse( f.keepUnchecked(r3) );

    }


    /**
     * Test filtering based on presence of specific atom
     */
    @Test
    public void testAtomFilter()
    {
        ReactionFilter f = new FilterBuilder().mustContainsAtoms("C").build();

        f.checkAndFlag(r1);
        assertFalse( r1.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertTrue( f.keep(r1) );
        assertTrue( f.keepUnchecked(r1) );

        f.checkAndFlag(r2);
        assertFalse( r2.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertTrue( f.keep(r2) );
        assertTrue( f.keepUnchecked(r2) );

        //-------------------------------

        f.checkAndFlag(r3);
        assertTrue( r3.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertFalse( f.keep(r3) );
        assertFalse( f.keepUnchecked(r3) );

        f.checkAndFlag(r4);
        assertTrue( r4.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertFalse( f.keep(r4) );
        assertFalse( f.keepUnchecked(r4) );

    }

    /**
     * Test filtering based on balanced number of atoms
     */
    @Test
    public void testBalanceFilter()
    {
        ReactionFilter f = new FilterBuilder().onlyBalanced().build();

        assertEquals(f.getNumberOfAtoms(r1.getSubstrates()), 11);
        assertEquals(f.getNumberOfAtoms(r1.getProducts()), 11);

        f.checkAndFlag(r1);
        assertFalse( r1.getFlags().contains(Flag.NON_BALANCED));
        assertTrue( f.keep(r1) );
        assertTrue( f.keepUnchecked(r1) );

        f.checkAndFlag(r4);
        assertFalse( r4.getFlags().contains(Flag.NON_BALANCED));
        assertTrue( f.keep(r4) );
        assertTrue( f.keepUnchecked(r4) );

        //-------------------------------

        f.checkAndFlag(r2);
        assertTrue( r2.getFlags().contains(Flag.NON_BALANCED));
        assertFalse( f.keep(r2) );
        assertFalse( f.keepUnchecked(r2) );

        f.checkAndFlag(r3);
        assertTrue( r3.getFlags().contains(Flag.NON_BALANCED));
        assertFalse( f.keep(r3) );
        assertFalse( f.keepUnchecked(r3) );
    }

    /**
     * Test filtering based on presence of specific atom
     */
    @Test
    public void testMultipleConditions()
    {
        ReactionFilter f = new FilterBuilder()
                .mustContainsAtoms("C")
                .maxCountOfParticipants(10)
                .maxCountOfTotalAtoms(1000)
                .onlyBalanced()
                .build();

        f.checkAndFlag(r1);
        assertFalse( r1.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertFalse( r1.getFlags().contains(Flag.NON_BALANCED));
        assertFalse( r1.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertFalse( r1.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertTrue( f.keep(r1) );
        assertTrue( f.keepUnchecked(r1) );

        //-------------------------------

        f.checkAndFlag(r2);
        assertFalse( r2.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertTrue( r2.getFlags().contains(Flag.NON_BALANCED));
        assertTrue( r2.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertTrue( r2.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertFalse( f.keep(r2) );
        assertFalse( f.keepUnchecked(r2) );

        f.checkAndFlag(r3);
        assertTrue( r3.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertTrue( r3.getFlags().contains(Flag.NON_BALANCED));
        assertFalse( r3.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertFalse( r3.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertFalse( f.keep(r3) );
        assertFalse( f.keepUnchecked(r3) );

        f.checkAndFlag(r4);
        assertTrue( r4.getFlags().contains(Flag.DO_NOT_CONTAINS_ATOM_TYPE));
        assertFalse( r4.getFlags().contains(Flag.NON_BALANCED));
        assertFalse( r4.getFlags().contains(Flag.TOO_MANY_PARTICIPANT));
        assertFalse( r4.getFlags().contains(Flag.TOO_MANY_ATOMS));
        assertFalse( f.keep(r4) );
        assertFalse( f.keepUnchecked(r4) );

    }
}

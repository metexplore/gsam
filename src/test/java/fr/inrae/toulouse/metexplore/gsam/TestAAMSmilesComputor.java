package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.AAM.AAMSmilesComputor;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import org.openscience.cdk.exception.CDKException;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit test for simple AAMSmilesComputor.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestAAMSmilesComputor {

    static GSAMReaction r1;
    static GSAMReaction r2;

    /**
     * create all reactions to filter
     */
    @BeforeAll
    public void init() {
        try {
            /*Isocitrate dehydrogenase*/
            GSAMCompound a = new GSAMCompound(new Species("M_nadp"), "C5(/C=C\\C(\\C(=O)N)=C/[N+](/[C@@H]1(O[C@@H]([C@H]([C@H]1O)O)COP(OP(OC[C@H]4(O[C@@H](N2(C3(\\C(\\N=C/2)=C(N)/N=C\\N=3)))[C@H](OP([O-])(=O)[O-])[C@H](O)4))(=O)[O-])(=O)[O-]))=5)");
            GSAMCompound b = new GSAMCompound(new Species("M_icit"), "C(=O)([C@@H](CC([O-])=O)[C@@H](O)C([O-])=O)[O-]");

            GSAMCompound c = new GSAMCompound(new Species("M_nadph"), "C1(\\N(/C=C(C\\C=1)/C(=O)N)[C@@H]2(O[C@@H]([C@H]([C@H]2O)O)COP(OP(OC[C@H]5(O[C@@H](N3(C4(\\C(\\N=C/3)=C(N)/N=C\\N=4)))[C@H](OP([O-])(=O)[O-])[C@H](O)5))(=O)[O-])(=O)[O-]))");
            GSAMCompound d = new GSAMCompound(new Species("M_akg"), "C(CC([O-])=O)C(=O)C([O-])=O");
            GSAMCompound e = new GSAMCompound(new Species("M_co2"), "C(=O)=O");
            /*
            REACTION - ISOCITDEH-RXN
            NTH-ATOM-MAPPING - 1
            MAPPING-TYPE - NO-HYDROGEN-ENCODING
            FROM-SIDE - (2-KETOGLUTARATE 0 9) (CARBON-DIOXIDE 10 12)
            TO-SIDE - (THREO-DS-ISO-CITRATE 0 12)
            INDICES - 1 0 3 2 10 4 7 6 5 12 11 9 8
             */
            HashMap<GSAMCompound, Integer> sr1 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr1 = new HashMap<>();
            sr1.put(a, 1);
            sr1.put(b, 1);
            pr1.put(c, 1);
            pr1.put(d, 1);
            pr1.put(e, 1);
            r1 = new GSAMReaction(new Reaction("R_ICDHyp"), sr1, pr1);


            /*erricytochrome-c 2-oxidoreductase*/
            /*
            HMR/Human1:R_HMR_3859
            KEGG:R00197

            reactant with stoichio >1
            react without SMILES
             */
            GSAMCompound f = new GSAMCompound(new Species("C00256"), "C[C@@H](O)C([O-])=O");
//            Compound g = new Compound(new Species("C00125"), "CC1=C(CCC(O)=O)C2=CC3=[N+]4C(=CC5=C(C)C(C=C)=C6C=C7C(C)=C(C)C8=[N+]7[Fe]4(N2C1=C8)N56)C(C)=C3CCC(O)=O");
            GSAMCompound g = new GSAMCompound(new Species("C00125"), "*");

            GSAMCompound h = new GSAMCompound(new Species("C00022"), "CC(=O)C([O-])=O");
            GSAMCompound i = new GSAMCompound(new Species("C00126"), "*");

            HashMap<GSAMCompound, Integer> sr2 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr2 = new HashMap<>();
            sr2.put(f, 1);
            sr2.put(g, 2);
            pr2.put(h, 1);
            pr2.put(i, 2);
            r2 = new GSAMReaction(new Reaction("R00197"), sr2, pr2);


        } catch (CDKException e) {
            e.printStackTrace();

        }

    }


    /**
     * Test filtering based on atoms count
     */
    @Test
    public void testComputorSimpleRxn() throws Exception {
        GSAMReaction r = r1;
        int atomsCount = 5+5+1+2+21+7+17+3;

        AAMSmilesComputor computor = new AAMSmilesComputor();
        String aam = computor.compute(r);

        //test RxnSmiles has substrates and products
        String[] SPaam = aam.split(">>");
        assertEquals(2, SPaam.length);

        //test RxnSmiles has right number of substrates
        String[] Saam = SPaam[0].split("\\.");
        assertEquals(r.getSubstrates().size(), Saam.length);

        //test RxnSmiles has right number of products
        String[] Paam = SPaam[1].split("\\.");
        assertEquals(r.getProducts().size(), Paam.length);

        //test Rxn has mapped atoms
        assertTrue(SPaam[0].matches(".*:\\d.*"));
        assertTrue(SPaam[1].matches(".*:\\d.*"));

        //extract mapping.
        HashMap<String,HashSet<Integer>> Satoms = new HashMap<>();
        HashMap<String,HashSet<Integer>> Patoms = new HashMap<>();
        Matcher Smatcher = Pattern.compile("\\[([^:]+):(\\d+)\\]").matcher(SPaam[0]);
        Matcher Pmatcher = Pattern.compile("\\[([^:]+):(\\d+)\\]").matcher(SPaam[1]);
        while (Smatcher.find()){
            String atomType = Smatcher.group(1);
            atomType = atomType.replaceAll("H\\d*", "");
            String atomLabel = Smatcher.group(2);
            HashSet<Integer> labels = Satoms.get(atomType);
            if(labels==null) labels = new HashSet<>();
            labels.add(Integer.parseInt(atomLabel));
            Satoms.put(atomType,labels);
        }
        while (Pmatcher.find()){
            String atomType = Pmatcher.group(1);
            atomType = atomType.replaceAll("H\\d*", "");
            String atomLabel = Pmatcher.group(2);
            HashSet<Integer> labels = Patoms.get(atomType);
            if(labels==null) labels = new HashSet<>();
            labels.add(Integer.parseInt(atomLabel));
            Patoms.put(atomType,labels);
        }

        //check mapping as right number of atoms
        int maxS = Collections.max(Satoms.values().stream().flatMap(Set::stream).collect(Collectors.toSet()));
        int maxP = Collections.max(Patoms.values().stream().flatMap(Set::stream).collect(Collectors.toSet()));
        assertEquals(atomsCount, maxS );
        assertEquals(atomsCount, maxP);

        //check same label shared between substrates and products
        //check labels are consistent between atom type (no [C:1] >> [N:1])
        assertEquals(Satoms,Patoms);
    }


    /**
     * Test filtering based on atoms count
     */
    @Test
    public void testComputorCplxRxn() throws Exception {
        GSAMReaction r = r2;
        int atomsCount = 8;

        AAMSmilesComputor computor = new AAMSmilesComputor();
        String aam = computor.compute(r);

        //test RxnSmiles has substrates and products
        String[] SPaam = aam.split(">>");
        assertEquals(2, SPaam.length);

        //test RxnSmiles has right number of substrates
        String[] Saam = SPaam[0].split("\\.");
        assertEquals(r.getSubstrates().size(), Saam.length);

        //test RxnSmiles has right number of products
        String[] Paam = SPaam[1].split("\\.");
        assertEquals(r.getProducts().size(), Paam.length);

        //test Rxn has mapped atoms
        assertTrue(SPaam[0].matches(".*:\\d.*"));
        assertTrue(SPaam[1].matches(".*:\\d.*"));

        //extract mapping.
        HashMap<String,HashSet<Integer>> Satoms = new HashMap<>();
        HashMap<String,HashSet<Integer>> Patoms = new HashMap<>();
        Matcher Smatcher = Pattern.compile("\\[([^:]+):(\\d+)\\]").matcher(SPaam[0]);
        Matcher Pmatcher = Pattern.compile("\\[([^:]+):(\\d+)\\]").matcher(SPaam[1]);
        while (Smatcher.find()){
            String atomType = Smatcher.group(1);
            atomType = atomType.replaceAll("H\\d*", "");
            String atomLabel = Smatcher.group(2);
            HashSet<Integer> labels = Satoms.get(atomType);
            if(labels==null) labels = new HashSet<>();
            labels.add(Integer.parseInt(atomLabel));
            Satoms.put(atomType,labels);
        }
        while (Pmatcher.find()){
            String atomType = Pmatcher.group(1);
            atomType = atomType.replaceAll("H\\d*", "");
            String atomLabel = Pmatcher.group(2);
            HashSet<Integer> labels = Patoms.get(atomType);
            if(labels==null) labels = new HashSet<>();
            labels.add(Integer.parseInt(atomLabel));
            Patoms.put(atomType,labels);
        }

        //check mapping as right number of atoms
        int maxS = Collections.max(Satoms.values().stream().flatMap(Set::stream).collect(Collectors.toSet()));
        int maxP = Collections.max(Patoms.values().stream().flatMap(Set::stream).collect(Collectors.toSet()));
        assertEquals(atomsCount, maxS );
        assertEquals(atomsCount, maxP);

        //check same label shared between substrates and products
        //check labels are consistent between atom type (no [C:1] >> [N:1])
        assertEquals(Satoms,Patoms);
    }

}
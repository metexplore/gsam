package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.AAM.AAMSmileParser;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMTransition;
import fr.inrae.toulouse.metexplore.gsam.utils.TransitionComputor;
import org.openscience.cdk.exception.CDKException;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;

import java.util.Collection;
import java.util.HashMap;


import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestTransitionComputor {
    static GSAMReaction r1,r2;
    static GSAMCompound a,b,c,d,e,f,g,h,i;


    /**
     * create all reactions to filter
     */
    @BeforeAll
    public void init() {
        try {
            AAMSmileParser parser = new AAMSmileParser();
            parser.setAtoms("C");

            /*Isocitrate dehydrogenase*/
            a = new GSAMCompound(new Species("M_nadp"), "C5(/C=C\\C(\\C(=O)N)=C/[N+](/[C@@H]1(O[C@@H]([C@H]([C@H]1O)O)COP(OP(OC[C@H]4(O[C@@H](N2(C3(\\C(\\N=C/2)=C(N)/N=C\\N=3)))[C@H](OP([O-])(=O)[O-])[C@H](O)4))(=O)[O-])(=O)[O-]))=5)");
            b = new GSAMCompound(new Species("M_icit"), "C(=O)([C@@H](CC([O-])=O)[C@@H](O)C([O-])=O)[O-]");

            c = new GSAMCompound(new Species("M_nadph"), "C1(\\N(/C=C(C\\C=1)/C(=O)N)[C@@H]2(O[C@@H]([C@H]([C@H]2O)O)COP(OP(OC[C@H]5(O[C@@H](N3(C4(\\C(\\N=C/3)=C(N)/N=C\\N=4)))[C@H](OP([O-])(=O)[O-])[C@H](O)5))(=O)[O-])(=O)[O-]))");
            d = new GSAMCompound(new Species("M_akg"), "C(CC([O-])=O)C(=O)C([O-])=O");
            e = new GSAMCompound(new Species("M_co2"), "C(=O)=O");

            /*
            REACTION - ISOCITDEH-RXN
            NTH-ATOM-MAPPING - 1
            MAPPING-TYPE - NO-HYDROGEN-ENCODING
            FROM-SIDE - (2-KETOGLUTARATE 0 9) (CARBON-DIOXIDE 10 12)
            TO-SIDE - (THREO-DS-ISO-CITRATE 0 12)
            INDICES - 1 0 3 2 10 4 7 6 5 12 11 9 8
             */
            HashMap<GSAMCompound, Integer> sr1 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr1 = new HashMap<>();
            sr1.put(a, 1);
            sr1.put(b, 1);
            pr1.put(c, 1);
            pr1.put(d, 1);
            pr1.put(e, 1);
            Reaction sbmlR1 = new Reaction("R_ICDHyp");
            sbmlR1.setReversible(false);
            r1 = new GSAMReaction(sbmlR1, sr1, pr1);

            String RxnSmiles1 =
                    "[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][CH:6]=[CH:7][N:8](=[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]4[CH:28]=[N:29][C:30]=5[C:31](=[N:32][CH:33]=[N:34][C:35]45)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]." +
                    "[O:49]=[C:50]([OH:51])[CH2:52][CH:53]([C:54](=[O:55])[OH:56])[CH:57]([OH:58])[C:59](=[O:60])[OH:61]" +
                    ">>" +
                    "[O:1]=[C:2]([NH2:3])[C:4]=1[CH2:5][CH:6]=[CH:7][N:8]([CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]4[CH:28]=[N:29][C:30]=5[C:31](=[N:32][CH:33]=[N:34][C:35]45)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]." +
                    "[O:49]=[C:50]([OH:51])[CH2:52][CH2:53][C:57](=[O:58])[C:59](=[O:60])[OH:61]." +
                    "[C:54](=[O:55])=[O:56]";

            parser.parseMapping(r1, RxnSmiles1);



            /*erricytochrome-c 2-oxidoreductase*/
            /*
            HMR/Human1:R_HMR_3859
            KEGG:R00197

            reactant with stoichio >1
            react without SMILES
             */
            f = new GSAMCompound(new Species("C00256"), "C[C@@H](O)C([O-])=O");
//            g = new Compound(new Species("C00125"), "CC1=C(CCC(O)=O)C2=CC3=[N+]4C(=CC5=C(C)C(C=C)=C6C=C7C(C)=C(C)C8=[N+]7[Fe]4(N2C1=C8)N56)C(C)=C3CCC(O)=O");
            g = new GSAMCompound(new Species("C00125"), "*");

            h = new GSAMCompound(new Species("C00022"), "CC(=O)C([O-])=O");
            i = new GSAMCompound(new Species("C00126"), "*");

            HashMap<GSAMCompound, Integer> sr2 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr2 = new HashMap<>();
            sr2.put(f, 1);
            sr2.put(g, 2);
            pr2.put(h, 1);
            pr2.put(i, 2);
            Reaction sbmlR2 = new Reaction("R00197");
            sbmlR2.setReversible(true);
            r2 = new GSAMReaction(sbmlR2, sr2, pr2);

            String RxnSmiles2 =
                    "[*:7]." +
                    "[*:8]." +
                    "[O:1]=[C:2]([OH:3])[CH:4]([OH:5])[CH3:6]" +
                    ">>" +
                    "[*:7]." +
                    "[*:8]." +
                    "[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH3:6]";
            parser.parseMapping(r2, RxnSmiles2);

        } catch (CDKException e) {
            e.printStackTrace();

        }

    }

    @Test
    public void testTransitionsFromSimpleRxn() {
        GSAMReaction r = r1;
        int nbOfTransitions = 3;
        GSAMTransition t1 = new GSAMTransition(a,c,r1);
        t1.computeAtomTransitions();
        GSAMTransition t2 = new GSAMTransition(b,d,r1);
        t2.computeAtomTransitions();
        GSAMTransition t3 = new GSAMTransition(b,e,r1);
        t3.computeAtomTransitions();

        Collection<GSAMTransition> transitions = TransitionComputor.reactionToTransitions(r);
        assertEquals(nbOfTransitions,transitions.size());
        assertTrue(transitions.contains(t1));
        assertTrue(transitions.contains(t2));
        assertTrue(transitions.contains(t3));
    }

    @Test
    public void testTransitionsFromCplxRxn() {
        GSAMReaction r = r2;
        int nbOfTransitions = 2;
        GSAMTransition t1 = new GSAMTransition(f,h,r2);
        t1.computeAtomTransitions();
        GSAMTransition t2 = new GSAMTransition(h,f,r2);
        t2.computeAtomTransitions();

        Collection<GSAMTransition> transitions = TransitionComputor.reactionToTransitions(r);
        assertEquals(nbOfTransitions,transitions.size());
        assertTrue(transitions.contains(t1));
        assertTrue(transitions.contains(t2));

    }
}

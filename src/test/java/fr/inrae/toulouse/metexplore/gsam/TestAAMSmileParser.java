package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.AAM.AAMSmileParser;
import fr.inrae.toulouse.metexplore.gsam.AAM.AtomMappingContainer;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openscience.cdk.exception.CDKException;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for simple AAMSmilesComputor.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestAAMSmileParser {

    static GSAMReaction r1;
    static GSAMReaction r2;

    /**
     * create all reactions to filter
     */
    @BeforeAll
    public void init() {
        try {
            /*Isocitrate dehydrogenase*/
            GSAMCompound a = new GSAMCompound(new Species("M_nadp"), "C5(/C=C\\C(\\C(=O)N)=C/[N+](/[C@@H]1(O[C@@H]([C@H]([C@H]1O)O)COP(OP(OC[C@H]4(O[C@@H](N2(C3(\\C(\\N=C/2)=C(N)/N=C\\N=3)))[C@H](OP([O-])(=O)[O-])[C@H](O)4))(=O)[O-])(=O)[O-]))=5)");
            GSAMCompound b = new GSAMCompound(new Species("M_icit"), "C(=O)([C@@H](CC([O-])=O)[C@@H](O)C([O-])=O)[O-]");

            GSAMCompound c = new GSAMCompound(new Species("M_nadph"), "C1(\\N(/C=C(C\\C=1)/C(=O)N)[C@@H]2(O[C@@H]([C@H]([C@H]2O)O)COP(OP(OC[C@H]5(O[C@@H](N3(C4(\\C(\\N=C/3)=C(N)/N=C\\N=4)))[C@H](OP([O-])(=O)[O-])[C@H](O)5))(=O)[O-])(=O)[O-]))");
            GSAMCompound d = new GSAMCompound(new Species("M_akg"), "C(CC([O-])=O)C(=O)C([O-])=O");
            GSAMCompound e = new GSAMCompound(new Species("M_co2"), "C(=O)=O");
            /*
            REACTION - ISOCITDEH-RXN
            NTH-ATOM-MAPPING - 1
            MAPPING-TYPE - NO-HYDROGEN-ENCODING
            FROM-SIDE - (2-KETOGLUTARATE 0 9) (CARBON-DIOXIDE 10 12)
            TO-SIDE - (THREO-DS-ISO-CITRATE 0 12)
            INDICES - 1 0 3 2 10 4 7 6 5 12 11 9 8
             */
            HashMap<GSAMCompound, Integer> sr1 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr1 = new HashMap<>();
            sr1.put(a, 1);
            sr1.put(b, 1);
            pr1.put(c, 1);
            pr1.put(d, 1);
            pr1.put(e, 1);
            r1 = new GSAMReaction(new Reaction("R_ICDHyp"), sr1, pr1);


            /*erricytochrome-c 2-oxidoreductase*/
            /*
            HMR/Human1:R_HMR_3859
            KEGG:R00197

            reactant with stoichio >1
            react without SMILES
             */
            GSAMCompound f = new GSAMCompound(new Species("C00256"), "C[C@@H](O)C([O-])=O");
//            Compound g = new Compound(new Species("C00125"), "CC1=C(CCC(O)=O)C2=CC3=[N+]4C(=CC5=C(C)C(C=C)=C6C=C7C(C)=C(C)C8=[N+]7[Fe]4(N2C1=C8)N56)C(C)=C3CCC(O)=O");
            GSAMCompound g = new GSAMCompound(new Species("C00125"), "*");

            GSAMCompound h = new GSAMCompound(new Species("C00022"), "CC(=O)C([O-])=O");
            GSAMCompound i = new GSAMCompound(new Species("C00126"), "*");

            HashMap<GSAMCompound, Integer> sr2 = new HashMap<>();
            HashMap<GSAMCompound, Integer> pr2 = new HashMap<>();
            sr2.put(f, 1);
            sr2.put(g, 2);
            pr2.put(h, 1);
            pr2.put(i, 2);
            r2 = new GSAMReaction(new Reaction("R00197"), sr2, pr2);


        } catch (CDKException e) {
            e.printStackTrace();

        }

    }


    /**
     * Test parsing of SMILES with atom mapping
     */
    @Test
    public void testParserCplxRxn() {
        GSAMReaction r = r2;
        int nSubstrates = 2;
        int nProducts = 2;
        int nbC = 3;
        String fullyMapped1 = "C00256";
        String fullyMapped2 = "C00022";
        String aamSMILES = "[*:7].[*:8].[O:1]=[C:2]([OH:3])[CH:4]([OH:5])[CH3:6]>>[*:7].[*:8].[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH3:6]";

        AAMSmileParser parser = new AAMSmileParser();
        parser.setAtoms("C");
        AtomMappingContainer res = new AtomMappingContainer();
        parser.parseMapping(r.getSubstrates(),r.getProducts(),aamSMILES, res);

        //check number of substrates and products
        assertEquals(nSubstrates,res.getReactants().size());
        assertEquals(nProducts,res.getProducts().size());

        //check labels shared between substrates and products
        Set<String> substratesLabels = res.getReactants().stream().collect(HashSet::new, (x,y) -> x.addAll(res.getReactantAtomLabels(y)), HashSet::addAll);
        Set<String> productsLabels = res.getProducts().stream().collect(HashSet::new, (x,y) -> x.addAll(res.getProductAtomLabels(y)), HashSet::addAll);
        assertNotNull(substratesLabels);
        assertNotNull(productsLabels);
        assertNotEquals(new HashSet<>(),substratesLabels);
        assertNotEquals(new HashSet<>(),productsLabels);
        assertEquals(substratesLabels,productsLabels);

        //check number of carbons
        assertEquals(nbC,substratesLabels.size());

        //check mapping
        assertNotNull(res.getReactantAtomLabels(fullyMapped1));
        assertNotNull(res.getProductAtomLabels(fullyMapped2));
        assertEquals(res.getReactantAtomLabels(fullyMapped1),res.getProductAtomLabels(fullyMapped2));

        //check conserved atoms extraction
        assertEquals(res.getReactantAtomLabels(fullyMapped1),res.getConservedAtoms(fullyMapped1,fullyMapped2));
    }

    /**
     * Test parsing of SMILES with atom mapping
     */
    @Test
    public void testParserSimpleRxn() {
        GSAMReaction r = r1;
        int nSubstrates = 2;
        int nProducts = 3;
        int nbC = 27;
        String fullyMapped1 = "M_nadp";
        String fullyMapped2 = "M_nadph";
        String aamSMILES = "[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][CH:6]=[CH:7][N:8](=[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]4[CH:28]=[N:29][C:30]=5[C:31](=[N:32][CH:33]=[N:34][C:35]45)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]([OH:51])[CH2:52][CH:53]([C:54](=[O:55])[OH:56])[CH:57]([OH:58])[C:59](=[O:60])[OH:61]>>[O:1]=[C:2]([NH2:3])[C:4]=1[CH2:5][CH:6]=[CH:7][N:8]([CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]4[CH:28]=[N:29][C:30]=5[C:31](=[N:32][CH:33]=[N:34][C:35]45)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]([OH:51])[CH2:52][CH2:53][C:57](=[O:58])[C:59](=[O:60])[OH:61].[C:54](=[O:55])=[O:56]";

        AAMSmileParser parser = new AAMSmileParser();
        parser.setAtoms("C");
        AtomMappingContainer res = new AtomMappingContainer();
        parser.parseMapping(r.getSubstrates(),r.getProducts(),aamSMILES, res);

        //check number of substrates and products
        assertEquals(nSubstrates,res.getReactants().size());
        assertEquals(nProducts,res.getProducts().size());

        //check labels shared between substrates and products
        Set<String> substratesLabels = res.getReactants().stream().collect(HashSet::new, (x,y) -> x.addAll(res.getReactantAtomLabels(y)), HashSet::addAll);
        Set<String> productsLabels = res.getProducts().stream().collect(HashSet::new, (x,y) -> x.addAll(res.getProductAtomLabels(y)), HashSet::addAll);
        assertNotNull(substratesLabels);
        assertNotNull(productsLabels);
        assertNotEquals(new HashSet<>(),substratesLabels);
        assertNotEquals(new HashSet<>(),productsLabels);
        assertEquals(substratesLabels,productsLabels);

        //check number of carbons
        assertEquals(nbC,substratesLabels.size());

        //check mapping
        assertNotNull(res.getReactantAtomLabels(fullyMapped1));
        assertNotNull(res.getProductAtomLabels(fullyMapped2));
        assertEquals(res.getReactantAtomLabels(fullyMapped1),res.getProductAtomLabels(fullyMapped2));

        //check conserved atoms extraction
        assertEquals(res.getReactantAtomLabels(fullyMapped1),res.getConservedAtoms(fullyMapped1,fullyMapped2));
    }
}
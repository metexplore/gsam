package fr.inrae.toulouse.metexplore.gsam.utils;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMTransition;
import org.openscience.cdk.exception.CDKException;
import org.sbml.jsbml.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ParseSBML {

    private static final Logger logger = LoggerFactory.getLogger(ParseSBML.class);

    /*
     * parse SBML using JSBML library
     */
    public static Model parseSbml(String pathToSbml) throws XMLStreamException, IOException {
        SBMLReader reader = new SBMLReader();
        SBMLDocument doc = reader.readSBMLFromFile(pathToSbml);
        Model model = doc.getModel(); //JSBML model stores all data from SBML file
        logger.info("SBML successfully imported");
        return model;
    }

    /*
     * Process information related to compounds
     */
    public static HashMap<String, GSAMCompound> createCompounds(Model model, Map<String, String> SMILEmap){

        //Our application requires information about compounds' mass, atomic composition, and chemical structure.
        //All this information will be extracted from SMILES molecular descriptor stored in our Compound class.
        HashMap<String, GSAMCompound> entries = new HashMap<>();

        //Create the compound objects.
        //The compound object store the sbml entry and add some extra information such as mass and smiles descriptor
        //The extra information are computed from the InChI descriptor that represent the compound structure
        //The mass will be used to compute transitions' mass shifts for each reaction
        //The smile will be used to compute the atom mapping between substrates and products of reactions
        ListOf<Species> species =  model.getListOfSpecies();
        int i = 0;
        for(Species s : species) {
            GSAMCompound cpd;

            String smile = SMILEmap.get(s.getId());
            if(smile!=null){
                try {
                    cpd=new GSAMCompound(s, smile);
                    i++;
                } catch (CDKException e) {
                    cpd=new GSAMCompound(s);
                    logger.error("[SMILES] "+s.getId()+": SMILES couldn't been parsed");
                    logger.debug("[SMILES] "+s.getId()+": "+smile);
                }

                //Ignore reactants solely composed of Hydrogen
                //Models commonly involve a H+ compound to balance reactions.
                //Since H atoms are ignored during atom mapping, this kind of species are removed from reactions
                if(!smile.matches(".*[A-Za-z&&[^H]].*")){
                    cpd=null;
                    logger.warn("[SMILES] "+s.getId()+" ignored: SMILES only contain Hydrogen");
                    logger.debug("[SMILES] "+s.getId()+": "+smile);
                }

            }else{
                cpd=new GSAMCompound(s);
            }


            entries.put(s.getId(), cpd);

        }

        logger.info(model.getListOfSpecies().size()+" Compounds imported from SBML");
        logger.info(i+" compound annotated with SMILE");

        return entries;

    }

    public static HashMap<String, GSAMReaction> createReactions(Model model, Map<String, GSAMCompound> entries){
        //##parse sbml reaction
        HashMap<String, GSAMReaction> reactions = new HashMap<>();
        model.getListOfReactions().forEach((rEntry) -> {
            HashMap<GSAMCompound, Integer> substrates = new HashMap<>();
            for (SpeciesReference sr : rEntry.getListOfReactants()) {
                int stoichio = (int) sr.getStoichiometry();
                GSAMCompound c = entries.get(sr.getSpeciesInstance().getId());
                if(c!=null) substrates.put(c, stoichio); //c can be null since H+ are removed
            }

            HashMap<GSAMCompound, Integer> products = new HashMap<>();
            for (SpeciesReference sp : rEntry.getListOfProducts()) {
                int stoichio = (int) sp.getStoichiometry();
                GSAMCompound c = entries.get(sp.getSpeciesInstance().getId());
                if(c!=null) products.put(c, stoichio); //c can be null since H+ are removed
            }

            try {
                if(!substrates.isEmpty() && !products.isEmpty()){ //ignore export reactions
                    GSAMReaction reaction = new GSAMReaction(rEntry, substrates, products);
                    reactions.put(rEntry.getId(), reaction);
                }
            }catch (Exception e){
                logger.error("[RXN] "+rEntry.getId()+": not able to create reaction");
                logger.trace("",e);
            }
        });

        return reactions;
    }


    public static HashMap<String, Collection<GSAMTransition>> createTransitions(HashMap<String, GSAMReaction> reactions){
        HashMap<String,Collection<GSAMTransition>> transitions = new HashMap<>();
        for(Map.Entry<String, GSAMReaction> e : reactions.entrySet()){
            GSAMReaction r = e.getValue();
            if(r.hasAtomMapping()) {
                Collection<GSAMTransition> rtransitions = TransitionComputor.reactionToTransitions(r);
                if (rtransitions != null && !rtransitions.isEmpty()) {
                    transitions.put(e.getKey(), rtransitions);
                }else{
                    transitions.put(e.getKey(), new ArrayList<>());
                    logger.warn("[RXN] "+e.getKey()+": no transitions found");
                }
            }else{
                transitions.put(e.getKey(), new ArrayList<>());
                logger.warn("[RXN] "+e.getKey()+": no mapping found");
            }
        }

        return transitions;
    }
}

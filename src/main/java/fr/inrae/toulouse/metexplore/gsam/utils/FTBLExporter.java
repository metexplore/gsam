package fr.inrae.toulouse.metexplore.gsam.utils;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMTransition;

import java.io.IOException;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class used to export GSAM output in format compatible with influx_si software for flux analysis from stable isotopes labeling data
 */
public class FTBLExporter {
    private Map<String,String> labelConversion= new HashMap<>();;
    private Queue<String> alphabet = new LinkedList<>();

    private Collection<GSAMReaction> reactions;

    public void setSideCompoundsToIgnore(Collection<String> sideCompoundsToIgnore) {
        this.sideCompoundsToIgnore = sideCompoundsToIgnore;
    }

    private Collection<String> sideCompoundsToIgnore;
    private Collection<String> sideCompoundsAtomLabels;
    private Map<String,Collection<GSAMTransition>> indexedTransitions;

    /**
     * create FTBML Exporter
     * @param reactions the set of reactions to use
     * @param indexedTransitions the aam transition indexed by reaction
     */
    public FTBLExporter(Collection<GSAMReaction> reactions, Map<String,Collection<GSAMTransition>> indexedTransitions){
        this.indexedTransitions=indexedTransitions;
        this.reactions=reactions;
        this.sideCompoundsToIgnore=new HashSet<>();
    }

    /**
     * Export AAM as FTBL-like file, with one reaction per line
     * @param w the output writer
     * @throws IOException
     */
    public void exportFTBL(Writer w) throws IOException {
        for (GSAMReaction r : reactions) {
            initLabelConversion();
            StringBuffer line = new StringBuffer(r.getSbmlReaction().getId() + ":");
            line.append("\t");

            Map<String, String> substratesMapping = new LinkedHashMap<>();
            Map<String, String> productMapping = new LinkedHashMap<>();
            List<String> substrates = r.getSubstrates().stream().map(x -> x.getSbmlSpecie().getId()).collect(Collectors.toList());

            boolean emptySub = true;
            for(GSAMTransition transition : indexedTransitions.get(r.getSbmlReaction().getId())){
                String source = transition.getSource().getSbmlSpecie().getId();

                //skip backward transition for reversible reactions
                if(substrates.contains(source)){
                    if(!sideCompoundsToIgnore.contains(source)){
                        emptySub=false;
                        substratesMapping.computeIfAbsent(source,v ->
                                source+" ("+
                                        convertReactantAtomLabels(transition.getSourceAtomLabels()).stream().map(
                                                        sublist -> sublist.stream()
                                                                .collect(Collectors.joining("")))
                                                .collect(Collectors.joining(") + "+source+" ("))
                                        +")");
                    }else{
                        sideCompoundsAtomLabels.addAll(transition.getSourceAtomLabels().stream()
                                .flatMap(List::stream)
                                .collect(Collectors.toList()));
                        substratesMapping.computeIfAbsent(source,v ->
                                source+" ()");
                    }

                }
            }
            boolean emptyProd = true;
            for(GSAMTransition transition : indexedTransitions.get(r.getSbmlReaction().getId())){
                String target = transition.getTarget().getSbmlSpecie().getId();

                //skip backward transition for reversible reactions
                if(!substrates.contains(target)){
                    if(!sideCompoundsToIgnore.contains(target)) {
                        emptyProd = false;
                        productMapping.computeIfAbsent(target, v ->
                                target + " (" +
                                        convertProductAtomLabels(transition.getTargetAtomLabels()).stream().map(
                                                        sublist -> sublist.stream()
                                                                .collect(Collectors.joining("")))
                                                .collect(Collectors.joining(") + " + target + " ("))
                                        + ")");
                    }else {
                        productMapping.computeIfAbsent(target, v ->
                                target + " ()");
                    }
                }
            }

            if(!emptySub && !emptyProd){
                line.append(String.join(" + ", substratesMapping.values()));
                line.append(r.getSbmlReaction().getReversible() ? " <-> " : " -> ");
                line.append(String.join(" + ", productMapping.values()));
                line.append("\n");

                w.write(line.toString());
            }
        }
    }

    /*
     * used for FTBL export
     */
    private void initLabelConversion(){
        labelConversion= new HashMap<>();
        sideCompoundsAtomLabels=new HashSet<>();
        alphabet = new LinkedList<>();
        alphabet.addAll(Arrays.asList("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("(?!^)")));
    }
    /*
     * used for FTBL export
     */
    private List<List<String>> convertReactantAtomLabels(List<List<String>> labels){

        List<List<String>> out = new ArrayList<>();
        for(List<String> reactantsLabel : labels){
            List<String> newLabels = new ArrayList<>();
            for(String label : reactantsLabel){
                String newlabel = alphabet.isEmpty() ? "*" : alphabet.poll();
                labelConversion.put(label,newlabel);
                newLabels.add(newlabel);
            }
            out.add(newLabels);
        }
        return out;
    }
    /*
     * used for FTBL export
     */
    public List<List<String>> convertProductAtomLabels(List<List<String>> labels){
        List<List<String>> out = new ArrayList<>();
        for(List<String> reactantsLabel : labels){
            List<String> newLabels = new ArrayList<>();
            for(String label : reactantsLabel){
                if(!sideCompoundsAtomLabels.contains(label)){
                    String newlabel = labelConversion.get(label);
                    if(newlabel==null) newlabel="?";
                    newLabels.add(newlabel);
                }
            }
            out.add(newLabels);
        }
        return out;
    }

    public Collection<GSAMReaction> getReactions() {
        return reactions;
    }

    public void setReactions(Collection<GSAMReaction> reactions) {
        this.reactions = reactions;
    }

    public Map<String, Collection<GSAMTransition>> getIndexedTransitions() {
        return indexedTransitions;
    }

    public void setIndexedTransitions(Map<String, Collection<GSAMTransition>> indexedTransitions) {
        this.indexedTransitions = indexedTransitions;
    }
}

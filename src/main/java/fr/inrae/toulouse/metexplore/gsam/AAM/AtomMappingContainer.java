package fr.inrae.toulouse.metexplore.gsam.AAM;

import java.util.*;
import java.util.stream.Collectors;

public class AtomMappingContainer {
	
	private LinkedHashMap<String, List<List<String>>> reactants;
	private LinkedHashMap<String, List<List<String>>> products;
	
	
	public AtomMappingContainer() {
		reactants = new LinkedHashMap<>();
		products = new LinkedHashMap<>();
	}

	public void createFromReverseOrientation(AtomMappingContainer reverse) {
		reactants = reverse.products;
		products = reverse.reactants;
	}
	
	public void addReactant(String compoundId, List<String> labels){
		if(!reactants.containsKey(compoundId)) reactants.put(compoundId, new ArrayList<>());
		this.reactants.get(compoundId).add(labels);
	}
	
	public void addProduct(String compoundId, List<String> labels){
		if(!products.containsKey(compoundId)) products.put(compoundId, new ArrayList<>());
		this.products.get(compoundId).add(labels);
	}

	
	public Set<String> getConservedAtoms(String reactantId, String productId){
		Set<String> reactantLabels = this.getReactantAtomLabels(reactantId);
		if(reactantLabels==null) return null;
		Set<String> productLabels = this.getProductAtomLabels(productId);
		if(productLabels==null) return null;

		Set<String> intersec = new HashSet<>(productLabels);
		intersec.retainAll(reactantLabels);
        return intersec;
	}
	
	public Set<String> getProductAtomLabels(String productId){
		if(!this.products.containsKey(productId)) return null;
		return products.get(productId).stream().flatMap(List::stream).collect(Collectors.toSet());
////TODO: check perf
//		return this.products.get(productId).stream()
//				.map(subSet -> subSet.stream().collect(Collectors.toSet()))
//				.collect(HashSet::new, Set::addAll, Set::addAll);
	}
	
	public Set<String> getReactantAtomLabels(String reactantId){
		if(!this.reactants.containsKey(reactantId)) return null;
		return reactants.get(reactantId).stream().flatMap(List::stream).collect(Collectors.toSet());
//		return this.reactants.get(reactantId).stream()
//				.map(subSet -> subSet.stream().collect(Collectors.toSet()))
//				.collect(HashSet::new, Set::addAll, Set::addAll);
	}

	public Set<String> getProducts(){
		return this.products.keySet();
	}

	public Set<String> getReactants(){
		return this.reactants.keySet();
	}


	public List<List<String>> getProductAtomLabelsSequence(String productId){
		return this.products.get(productId);
	}

	public List<List<String>> getReactantAtomLabelsSequence(String reactantId){
		return this.reactants.get(reactantId);
	}

	public Map<Integer,Integer> getMappedAtoms(String reactantId, String productId){
		if(reactants.get(reactantId)==null || products.get(productId)==null) return null;
		
		List<String> reactantAtms = reactants.get(reactantId).stream().collect(ArrayList::new, ArrayList::addAll, ArrayList::addAll);
		List<String> productAtms = products.get(productId).stream().collect(ArrayList::new, ArrayList::addAll, ArrayList::addAll);

		Map<String,Integer> reactantAtmsIndexes = new HashMap<>();
		for(int i = 0; i<reactantAtms.size(); i++){
			reactantAtmsIndexes.put(reactantAtms.get(i),i);
		}

		Map<String,Integer> productAtmsIndexes = new HashMap<>();
		for(int i = 0; i<productAtms.size(); i++){
			productAtmsIndexes.put(productAtms.get(i),i);
		}

		Map<Integer,Integer> mapped = new HashMap<>();
		for(String a : this.getConservedAtoms(reactantId,productId)){
			mapped.put(reactantAtmsIndexes.get(a),productAtmsIndexes.get(a));
		}

		return mapped;
	}
	
}

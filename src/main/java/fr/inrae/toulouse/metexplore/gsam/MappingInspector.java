package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IReaction;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;
import uk.ac.ebi.reactionblast.mechanism.MappingSolution;
import uk.ac.ebi.reactionblast.mechanism.ReactionMechanismTool;
import uk.ac.ebi.reactionblast.tools.ImageGenerator;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MappingInspector {

    @Option(name = "-s", aliases = {"--smile"}, usage = "input reaction SMILES", required = true)
    String smiles = null;
    @Option(name = "-o", aliases = {"--outdir"}, usage = "path to the output directory", required = true)
    String outputDir = null;

    //OPTIONS
//----------------
    @Option(name = "-all", usage = "if all mapping solutions should be exported", required = false)
    boolean all = false;

    //HELP
//----------------
    @Option(name = "-h", aliases = {"--help"},usage = "prints the help", required = false)
    private Boolean h = false;
    @Option(name = "-v", aliases = {"--version"},usage = "prints the version", required = false)
    private Boolean v = false;


    public static GSAMReaction SMILE2GSAMrxn(String smile){
        String[] splitSMILE = smile.split(">>");
        Map<String,Integer> substratesStochio = new HashMap<>(), productsStochio = new HashMap<>();
        for(String react : splitSMILE[0].split(".")){
            substratesStochio.merge(react, 1, Integer::sum);
        }
        Map<GSAMCompound,Integer> substrates = substratesStochio.entrySet()
                .stream()
                .collect(Collectors.toMap( e -> {
                            try {
                                return new GSAMCompound(new Species(),e.getKey());
                            } catch (CDKException ex) {
                                throw new RuntimeException(ex);
                            }
                        },
                        e -> e.getValue()));
        for(String prod : splitSMILE[1].split(".")){
            productsStochio.merge(prod, 1, Integer::sum);
        }
        Map<GSAMCompound,Integer> products = productsStochio.entrySet()
                .stream()
                .collect(Collectors.toMap( e -> {
                            try {
                                return new GSAMCompound(new Species(),e.getKey());
                            } catch (CDKException ex) {
                                throw new RuntimeException(ex);
                            }
                        },
                        e -> e.getValue()));

        return new GSAMReaction(new Reaction(),substrates,products);
    }

    public static void main(String[] args) throws Exception {
        MappingInspector cli = new MappingInspector();
        cli.parseArguments(args);
        cli.run();
    }

    public void run() throws Exception {

        String smile = this.smiles;
        boolean all = true;

        SmilesParser smilesParser = new SmilesParser(SilentChemObjectBuilder.getInstance());
        IReaction r = smilesParser.parseReactionSmiles(smile);
        boolean forceMapping = true;//Overrides any mapping present int the reaction
        boolean generate2D = true;//2D perception of the stereo centers
        boolean generate3D = false;//2D perception of the stereo centers
        ReactionMechanismTool rmt = new ReactionMechanismTool(r, forceMapping, generate2D, generate3D, true);

        SmilesGenerator smiGen = new SmilesGenerator(SmiFlavor.AtomAtomMap);
        if(all){
            int i=1;
            for(MappingSolution s: rmt.getAllSolutions()){
                File file = new File(this.outputDir);
                ImageGenerator gen = new ImageGenerator();
                gen.drawLeftToRightReactionLayout(file.getCanonicalPath(), s.getBondChangeCalculator().getReactionWithCompressUnChangedHydrogens(), "reaction_"+i);
                System.out.println("Solution "+i+" : \n"+smiGen.create(s.getReaction()));
                i++;
            }
        }else{
            MappingSolution s = rmt.getSelectedSolution();//Fetch the AAM Solution
            File file = new File(this.outputDir);
            ImageGenerator gen = new ImageGenerator();
            System.out.println("Solution : \n"+smiGen.create(s.getReaction()));
            gen.drawLeftToRightReactionLayout(file.getCanonicalPath(), s.getBondChangeCalculator().getReactionWithCompressUnChangedHydrogens(), "reaction");
        }

    }

    protected void parseArguments(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);

        try {
            parser.parseArgument(args);
            if(this.h){
                parser.printUsage(System.out);
                System.exit(0);
            }
        } catch (CmdLineException e) {
            if(this.h){
                parser.printUsage(System.out);
                System.exit(0);
            }
            else{
                System.err.println("Error in arguments\n");
                parser.printUsage(System.err);
                System.exit(1);
            }
        }
    }


}

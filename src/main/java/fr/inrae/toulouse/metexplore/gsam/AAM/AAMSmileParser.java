package fr.inrae.toulouse.metexplore.gsam.AAM;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//TODO : create enum for atoms

public class AAMSmileParser {

	List<String> atoms = Arrays.asList("H","B","C","N","O","F","Na","Mg","Al","Si","P","S","Cl","K","Ca","Cr","Mn","Fe","Co","Cu","Zn","Se","Mo","Cd","Sn","I");

	public void parseMapping(GSAMReaction r, String rxnSmile) throws IllegalArgumentException{
		AtomMappingContainer mapping = new AtomMappingContainer();
		parseMapping(r.getSubstrates(), r.getProducts(), rxnSmile, mapping);
		r.setAtomMapping(mapping);
	}

	public void parseMapping(List<GSAMCompound> reactantIds, List<GSAMCompound>productIds, String rxnSmile, AtomMappingContainer mapping) throws IllegalArgumentException{

		//separate reactant list from products list
		String[] split = rxnSmile.split(">>");
		if(split.length!=2) throw new IllegalArgumentException(rxnSmile+" not valid");
		String reactants = split[0];
		String products = split[1];
		
		//separate reactant
		split = reactants.split("\\.");
		Iterator<GSAMCompound> iterator = reactantIds.iterator();
		
		if(split.length!=reactantIds.size()) throw (new IllegalArgumentException("number of reactants do not match RxnSMILES"));
		
		for(String reactant : split){

			String reactantId = iterator.next().getSbmlSpecie().getId();
			//extract atom labels
			List<String> atomLabels = getAtomLabelsFromSMILE(reactant);

			mapping.addReactant(reactantId, atomLabels);
		}

		
		//separate products
		split = products.split("\\.");
		iterator = productIds.iterator();
		
		if(split.length!=productIds.size()) throw (new IllegalArgumentException("number of products do not match RxnSMILES"));
		
		for(String product : split){

			String productId = iterator.next().getSbmlSpecie().getId();
			//extract atom labels
			List<String> atomLabels = getAtomLabelsFromSMILE(product);
			
			//check if the same reactant is present several times (due to stochiometry)
			mapping.addProduct(productId, atomLabels);
		}
	}
	
	private List<String> getAtomLabelsFromSMILE(String smilePart){
		ArrayList<String> atomLabels = new ArrayList<>();
		//separate atoms
		Matcher m = Pattern.compile(".*\\[([^\\]]+)\\].*").matcher(smilePart);
		while(m.matches()){
			String atom = m.group(1);
			
			//get atom label
			Matcher m2 = Pattern.compile("(.+):(\\d+)").matcher(atom);
			if(m2.matches()){
				//extract atom type
				String atomType = m2.group(1);
				atomType = atomType.replaceAll("H?\\d+", "");
				atomType = atomType.replaceAll("(.+)H?[+-]", "$1");
				atomType = atomType.replaceAll("(.+)H$", "$1");
				
				if(atoms.contains(atomType)){
					atomLabels.add(m2.group(2));
				}		
			}
			
			smilePart=smilePart.replace("["+atom+"]", "");
			m = Pattern.compile(".*\\[([^\\]]+)\\].*").matcher(smilePart);
		}
		return atomLabels;
	}

	/**
	 * @return the atoms
	 */
	public List<String> getAtoms() {
		return atoms;
	}

	/**
	 * @param atoms the atoms to set
	 */
	public void setAtoms(List<String> atoms) {
		this.atoms = atoms;
	}
	
	public void setAtoms(String atom) {
		this.atoms = new ArrayList<>();
		this.atoms.add(atom);
	}
	
}

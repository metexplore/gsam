package fr.inrae.toulouse.metexplore.gsam.data;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import fr.inrae.toulouse.metexplore.gsam.AAM.AtomMappingContainer;


public class GSAMTransition {
	
	private final GSAMCompound source;
	private final GSAMCompound target;
	private final GSAMReaction reaction;
	private double massshift = Double.NaN;
	private List<List<String>> sourceAtomLabels;
	private List<List<String>> targetAtomLabels;
	private Set<String> conservedAtomLabels;
	
	public GSAMTransition(GSAMCompound source, GSAMCompound target, GSAMReaction reaction){
		this.source=source;
		this.target=target;
		this.reaction=reaction;
		this.massshift=Math.abs(source.getMass()-target.getMass());
	}
	
	public void computeAtomTransitions() throws IllegalArgumentException{
		if(reaction.getAtomMapping()!=null){
			AtomMappingContainer aam = reaction.getAtomMapping();
			if(reaction.getSbmlReaction()!=null & reaction.getSbmlReaction().isReversible()){
				if(!reaction.getSubstrates().contains(source) & reaction.getProducts().contains(source)){
					aam = new AtomMappingContainer();
					aam.createFromReverseOrientation(reaction.getAtomMapping());
				}
			}
			this.computeAtomTransitions(aam);
		}else{
			throw new IllegalArgumentException("Reaction has no Atom-Atom mapping");
		}
	}

	public void computeAtomTransitions(AtomMappingContainer aam){
		String sourceId = this.source.getSbmlSpecie().getId();
		String targetId = this.target.getSbmlSpecie().getId();
		this.sourceAtomLabels = aam.getReactantAtomLabelsSequence(sourceId);
		this.targetAtomLabels = aam.getProductAtomLabelsSequence(targetId);
		this.conservedAtomLabels = aam.getConservedAtoms(sourceId,targetId);
	}

	/**
	 * @return the source
	 */
	public GSAMCompound getSource() {
		return source;
	}

	/**
	 * @return the target
	 */
	public GSAMCompound getTarget() {
		return target;
	}

	/**
	 * @return the reaction
	 */
	public GSAMReaction getReaction() {
		return reaction;
	}

	/**
	 * @return the massshift
	 */
	public double getMassshift() {
		return massshift;
	}

	/**
	 * @return the sourceAtomLabels
	 */
	public List<List<String>> getSourceAtomLabels() {
		return sourceAtomLabels;
	}

	/**
	 * @return the targetAtomLabels
	 */
	public List<List<String>> getTargetAtomLabels() {
		return targetAtomLabels;
	}

	/**
	 * @return the conservedAtomLabels
	 */
	public Set<String> getConservedAtomLabels() {
		return conservedAtomLabels;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GSAMTransition that = (GSAMTransition) o;
		return Double.compare(that.massshift, massshift) == 0 &&
				source.equals(that.source) &&
				target.equals(that.target) &&
				reaction.equals(that.reaction) &&
				Objects.equals(sourceAtomLabels, that.sourceAtomLabels) &&
				Objects.equals(targetAtomLabels, that.targetAtomLabels) &&
				Objects.equals(conservedAtomLabels, that.conservedAtomLabels);
	}

	@Override
	public int hashCode() {
		return Objects.hash(source, target, reaction, massshift, sourceAtomLabels, targetAtomLabels, conservedAtomLabels);
	}

}

package fr.inrae.toulouse.metexplore.gsam.data;

import java.util.Arrays;

import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IMolecularFormula;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.MolecularFormulaManipulator;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GSAMCompound {

	private static final Logger logger = LoggerFactory.getLogger(GSAMCompound.class);


	private final Species sbmlSpecie;
	private String smile="*H";
	private Double mass=Double.NaN;

	private final IAtomContainer m;

	public GSAMCompound(Species sbmlSpecie){
		this.sbmlSpecie=sbmlSpecie;
		m=new AtomContainer();
	}
	
	public GSAMCompound(Species sbmlSpecie, String smile) throws CDKException {
		this.sbmlSpecie = sbmlSpecie;
		if(smile.contains(".")){
			String[] smileParts = smile.split("\\.");
			Arrays.sort(smileParts, (a,b)->b.length() - a.length());
			logger.warn("[SMILES] "+sbmlSpecie.getId()+"have non connexe SMILES only longest part kept: "+smileParts[0]+". Create explicit reactant(s) from other parts if required");
			logger.debug("[SMILES] "+sbmlSpecie.getId()+": "+smile);
			smile=smileParts[0];
		}

		if(!smile.equals("*")){
			m = parseSmile(smile);
			//canonicalize smiles
			SmilesGenerator generator = new SmilesGenerator(SmiFlavor.Unique);
			this.smile = generator.create(m);
		}else{
			this.smile = "*H"; //create non empty SMILES
			SmilesParser sp  = new SmilesParser(SilentChemObjectBuilder.getInstance());
			m  = sp.parseSmiles(this.smile);
		}
		mass = computeMass(m);
	}

	private IAtomContainer parseSmile(String smile) throws CDKException {
		SmilesParser sp  = new SmilesParser(SilentChemObjectBuilder.getInstance());
		IAtomContainer m  = sp.parseSmiles(smile);
		//remove hydrogenes
		m = AtomContainerManipulator.removeHydrogens(m);
		//remove charge
		m.atoms().forEach(a -> a.setFormalCharge(0));
		//configure atoms
		AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(m);
		//add hydrogenes back as implicit ones
		CDKHydrogenAdder hAdder = CDKHydrogenAdder.getInstance(SilentChemObjectBuilder.getInstance());
		hAdder.addImplicitHydrogens(m);

		return m;
	}


	private Double computeMass(IAtomContainer m){
		IMolecularFormula formula = MolecularFormulaManipulator.getMolecularFormula(m);
		double mass = MolecularFormulaManipulator.getMajorIsotopeMass(formula);
//		double mass = MolecularFormulaManipulator.getNaturalExactMass(formula);
//		double mass = MolecularFormulaManipulator.getTotalExactMass(formula);
		return mass;
	}

	/**
	 * @return the sbmlSpecie
	 */
	public Species getSbmlSpecie() {
		return sbmlSpecie;
	}

	/**
	 * @return the smile
	 */
	public String getSmile() {
		return smile;
	}

	/**
	 * @return the mass
	 */
	public Double getMass() {
		return mass;
	}

	public IAtomContainer getCDKmolecule() { return m; }
	
	
}

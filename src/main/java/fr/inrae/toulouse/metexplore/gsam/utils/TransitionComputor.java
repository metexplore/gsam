package fr.inrae.toulouse.metexplore.gsam.utils;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMTransition;

import java.util.*;

public class TransitionComputor {

    public static Collection<GSAMTransition> reactionToTransitions(GSAMReaction r){
        List<GSAMTransition> transitions = new ArrayList<>();

        Set<GSAMCompound> substrates = new HashSet<>(r.getSubstrates());
        Set<GSAMCompound> products = new HashSet<>(r.getProducts());

        for(GSAMCompound substrate : substrates){
            for(GSAMCompound product : products){
                GSAMTransition transition = new GSAMTransition(substrate, product, r);
                transition.computeAtomTransitions();
                if(!transition.getConservedAtomLabels().isEmpty()){
                    transitions.add(transition);
                }
            }
        }

        if(r.getSbmlReaction()!=null && r.getSbmlReaction().getReversible()==true){
            List<GSAMTransition> reverses = new ArrayList<>();
            for(GSAMTransition t : transitions){
                GSAMTransition rt = new GSAMTransition(t.getTarget(),t.getSource(),r);
                rt.computeAtomTransitions();

                reverses.add(rt);
            }
            transitions.addAll(reverses);
        }
        return transitions;
    }
}

package fr.inrae.toulouse.metexplore.gsam.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Pattern;

public class FilterBuilder {

    int maxNbOfParticipant = Integer.MAX_VALUE;
    int maxNbOfAtoms = Integer.MAX_VALUE;
    boolean onlyBalanced = false;
    Pattern smileRegex = null;

    public FilterBuilder(){
    }

    public FilterBuilder maxCountOfParticipants (int maxNbOfParticipants){
        this.maxNbOfParticipant=maxNbOfParticipants;
        return this;
    }
    public FilterBuilder maxCountOfTotalAtoms (int maxNbOfAtoms){
        this.maxNbOfAtoms = maxNbOfAtoms;
        return this;
    }

    public FilterBuilder onlyBalanced(){
        this.onlyBalanced=true;
        return this;
    }

    public FilterBuilder mustContainsAtoms(String... atoms){
        smileRegex=buildAtomLookupRegex(atoms);
        return this;
    }

    private Pattern buildAtomLookupRegex(String... atoms){
        Collection<String> atomsPattern = new HashSet<>();
        for(String atom : atoms){
            //makes sure that HCl- or Ca2+ are not counted as having carbons despite "C" being found in formula
            if(atom.length()==1){
                atomsPattern.add(atom+"(?![a-z])");
            }else{
                atomsPattern.add(atom);
            }
        }
        String regex =  ".*("+String.join("|", atomsPattern)+").*";
        return Pattern.compile(regex);
    }

    public ReactionFilter build(){
        return new ReactionFilter(maxNbOfParticipant,maxNbOfAtoms,onlyBalanced,smileRegex);
    }


}

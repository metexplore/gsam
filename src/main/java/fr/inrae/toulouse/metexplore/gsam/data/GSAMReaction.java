package fr.inrae.toulouse.metexplore.gsam.data;

import fr.inrae.toulouse.metexplore.gsam.AAM.AtomMappingContainer;
import fr.inrae.toulouse.metexplore.gsam.utils.Flag;

import java.util.*;
import java.util.stream.Collectors;

import org.sbml.jsbml.Reaction;
import uk.ac.ebi.reactionblast.tools.AtomContainerSetComparator;

public class GSAMReaction {
	
	private final Reaction sbmlReaction;
	private final List<GSAMCompound> substrates;
	private final List<GSAMCompound> products;

	//this reactions is meant to be used to compute atom mapping. If not eligible to such computation, the reason should be stored here using the @Flag enum
	private EnumSet<Flag> flags;
	
	private String rxnSmiles;
	private AtomMappingContainer AAM;

	private String equation = "";

	//The creation of reaction will generate an explicit list of participants, with duplicates according to stoechiometry coeff.
	//The RxnSmiles is build at the instantiation of the reaction.
	public GSAMReaction(Reaction sbmlReaction, Map<GSAMCompound,Integer> substratesStochio, Map<GSAMCompound,Integer> productsStochio){
		this.flags=EnumSet.noneOf(Flag.class);
		this.sbmlReaction = sbmlReaction;
		this.substrates = new ArrayList<>();
		Map<GSAMCompound, List<Integer>> substratesIndexes = new HashMap<>();
		Map<GSAMCompound, List<Integer>> productsIndexes = new HashMap<>();
		
		List<GSAMCompound> substratesOrderedKeys = new ArrayList<>(substratesStochio.keySet());
		List<GSAMCompound> productsOrderedKeys = new ArrayList<>(productsStochio.keySet());
		CompoundComparator sorter = new CompoundComparator();
		substratesOrderedKeys.sort(sorter);
		productsOrderedKeys.sort(sorter);
		
		
		int i = 0;
		for(GSAMCompound substrate : substratesOrderedKeys){
			List<Integer> indexes = new ArrayList<>();
			for(int n=0; n<substratesStochio.get(substrate); n++){
				this.substrates.add(substrate);
				indexes.add(i);
				i++;
			}			
			substratesIndexes.put(substrate, indexes);
		}
		
		this.products = new ArrayList<>();
		i = 0;
		for(GSAMCompound product : productsOrderedKeys){
			List<Integer> indexes = new ArrayList<>();
			for(int n=0; n<productsStochio.get(product); n++){
				this.products.add(product);
				indexes.add(i);
				i++;
			}
			productsIndexes.put(product,indexes);
		}
		
		this.rxnSmiles = createRxnSmiles(this.substrates, this.products);
		this.equation = createEquationString(substratesStochio,productsStochio);
	}

	private String createEquationString(Map<GSAMCompound,Integer> substratesStochio, Map<GSAMCompound,Integer> productsStochio){
		String eq = "";
		eq = eq+substratesStochio.entrySet().stream()
				.map(e -> e.getValue()+" "+e.getKey().getSbmlSpecie().getId())
				.collect(Collectors.joining(" + "));
		eq = sbmlReaction.isReversible() ? eq+" <-> " : eq+" -> ";
		eq = eq+productsStochio.entrySet().stream()
				.map(e -> e.getValue()+" "+e.getKey().getSbmlSpecie().getId())
				.collect(Collectors.joining(" + "));
		return eq;
	}
	
	private String createRxnSmiles(List<GSAMCompound> substrates, List<GSAMCompound> products){
		String substratesSmiles = "";
		String productSmiles = "";

		for(GSAMCompound substrate : substrates){
			if(substratesSmiles.isEmpty()){
				substratesSmiles = substrate.getSmile();
			}else{
				substratesSmiles = substratesSmiles+"."+substrate.getSmile();
			}
		}
		
		for(GSAMCompound product : products){
			if(productSmiles.isEmpty()){
				productSmiles = product.getSmile();
			}else{
				productSmiles = productSmiles+"."+product.getSmile();
			}
		}
		
		String rxnSmiles = substratesSmiles+">>"+productSmiles;
		
		return rxnSmiles;
	}

//TODO
//	public String computeAtomMapping(String rxnSmile){
//		return "";
//	}
	

	
	public static class CompoundComparator implements Comparator<GSAMCompound>{

		public int compare(GSAMCompound o1, GSAMCompound o2) {
			//handle case when one compound has unknown structure
			if(o1.getSmile().contains("*")){
				if(o2.getSmile().contains("*")){
					return 0;
				}else{
					return -1;
				}
			}else if(o2.getSmile().contains("*")){
				return 1;
			}
			return new AtomContainerSetComparator().compare(o1.getCDKmolecule(), o2.getCDKmolecule());
		}
		
	}

	public boolean hasAtomMapping() { return AAM!=null; }
	

	/**
	 * @return the sbmlReaction
	 */
	public Reaction getSbmlReaction() {
		return sbmlReaction;
	}

	/**
	 * @return the substrates
	 */
	public List<GSAMCompound> getSubstrates() {
		return substrates;
	}

	/**
	 * @return the products
	 */
	public List<GSAMCompound> getProducts() {
		return products;
	}

	/**
	 * @return the rxnSmiles
	 */
	public String getRxnSmiles() {
		return rxnSmiles;
	}
	public void setRxnSmiles(String rxnSmiles) {
		this.rxnSmiles=rxnSmiles;
	}
	
	
	/**
	 * @return the atom mapping
	 */
	public AtomMappingContainer getAtomMapping() {
		return AAM;
	}

	/**
	 * @param atomMapping the atom Mapping to set
	 */
	public void setAtomMapping(AtomMappingContainer atomMapping) {
		this.AAM = atomMapping;
	}

	public EnumSet<Flag> getFlags(){
		return flags;
	}

	public void setFlags(EnumSet<Flag> flags){
		this.flags=flags;
	}

	public void flag(Flag flag){
		this.flags.add(flag);
	}

	public String getEquation() {
		return equation;
	}
}

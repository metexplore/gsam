package fr.inrae.toulouse.metexplore.gsam.AAM;

import java.util.*;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;

import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.interfaces.IReaction;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.*;

import uk.ac.ebi.reactionblast.mechanism.MappingSolution;
import uk.ac.ebi.reactionblast.mechanism.ReactionMechanismTool;
import uk.ac.ebi.reactionblast.tools.AtomContainerSetComparator;


public class AAMSmilesComputor 
{

	public HashMap<String,Set<String>> getSMILESmap() {
		return SMILESmap;
	}

	//keep track of uniq SMILES created, if more smiles than compounds it means some ended up with different SMILES, which means canonicalization failed
	HashMap<String,Set<String>> SMILESmap = new HashMap<>();

	public String compute(GSAMReaction r) throws Exception {
		String AAMsmiles = "?";
		IReaction cdkReaction = convertToCDKReaction(r);
		IReaction cdkReactionWithMapping = performAtomAtomMapping(cdkReaction);
		AAMsmiles = generateAAMSmiles(cdkReactionWithMapping,r);
		return AAMsmiles;
    }
	
	private IReaction convertToCDKReaction(GSAMReaction r) throws InvalidSmilesException{
	    SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
		IReaction cdkReaction = smilesParser.parseReactionSmiles(r.getRxnSmiles());
		cdkReaction.setID(r.getSbmlReaction().getId());
		return cdkReaction;
	}


	private IReaction performAtomAtomMapping(IReaction cdkReaction) throws Exception, AssertionError {
        boolean forceMapping = true;//Overrides any mapping present int the reaction
        boolean generate2D = true;//2D perception of the stereo centers
        boolean generate3D = false;//2D perception of the stereo centers
		ReactionMechanismTool rmt = new ReactionMechanismTool(cdkReaction, forceMapping, generate2D, generate3D, true);
        MappingSolution s = rmt.getSelectedSolution();//Fetch the AAM Solution
		return s.getReaction();
    }
    
    private String generateAAMSmiles(IReaction cdkReaction, GSAMReaction rxn) throws CDKException{
    	SmilesGenerator smilesGenerator = new SmilesGenerator(SmiFlavor.AtomAtomMap);
    	String AAMsmiles = smilesGenerator.create(cdkReaction);

    	//reorder compounds in smiles
    	String[] split = AAMsmiles.split(">>");
		List<String> reactants = new ArrayList<>(Arrays.asList(split[0].split("\\.")));
		List<String> products = new ArrayList<>(Arrays.asList(split[1].split("\\.")));
		reactants.sort(new SmileComparator());
		products.sort(new SmileComparator());

		for(int i=0; i< reactants.size();i++){
			reactants.set(i, canonicalizeSMILE(reactants.get(i),rxn.getSubstrates().get(i).getSbmlSpecie().getId()));
		}
		for(int i=0; i< products.size();i++){
			products.set(i, canonicalizeSMILE(products.get(i),rxn.getProducts().get(i).getSbmlSpecie().getId()));
		}

//		reactants = reactants.stream().map(this::canonicalizeSMILE).collect(Collectors.toList());
//		products = products.stream().map(this::canonicalizeSMILE).collect(Collectors.toList());
		AAMsmiles = String.join(".", reactants)+">>"+String.join(".", products);
		
    	return AAMsmiles;
    }
    
	public String canonicalizeSMILE(String smile, String id){
		//remove mapped explicit H
		smile = smile.replaceAll("\\[H:\\d+\\]", "");
		IChemObjectBuilder builder    = SilentChemObjectBuilder.getInstance();
		SmilesGenerator generator = new SmilesGenerator(SmiFlavor.UniversalSmiles|SmiFlavor.AtomAtomMap);
		SmilesParser parser = new SmilesParser(builder);
		String newSmile = smile;
		try {
			newSmile = generator.create(parser.parseSmiles(smile));
		} catch (CDKException e) {
			e.printStackTrace();
		}
		SMILESmap.computeIfAbsent(id, set-> new HashSet<>()).add(newSmile.replaceAll(":\\d+",""));
		return newSmile;
	}
    
    public static class SmileComparator implements Comparator<String>{
		
		public int compare(String o1, String o2) {
			if(o1.contains(".")) o1=o1.split("\\.")[0];
			if(o2.contains(".")) o2=o2.split("\\.")[0];

			if(o1.contains("*")){
				if(o2.contains("*")){
					return 0;
				}else{
					return -1;
				}
			}else if(o2.contains("*")){
				return 1;
			}

			SmilesParser smipar = new SmilesParser(SilentChemObjectBuilder.getInstance());
			
			try {
				Comparator<IAtomContainer> atomContainerComparator = new AtomContainerSetComparator();
				IAtomContainer ac1 = smipar.parseSmiles(o1);
				IAtomContainer ac2 = smipar.parseSmiles(o2);
				return atomContainerComparator.compare(ac1, ac2);
			} catch (InvalidSmilesException e) {
				e.printStackTrace();
				return 0;
			}	
		}
	}
}

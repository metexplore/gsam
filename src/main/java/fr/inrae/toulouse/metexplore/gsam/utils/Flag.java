package fr.inrae.toulouse.metexplore.gsam.utils;

public enum Flag {

    TOO_MANY_ATOMS("too many atoms"),
    NON_BALANCED("not balanced"),
    DO_NOT_CONTAINS_ATOM_TYPE("no atom to track"),
    MISSING_PARTICIPANT_SMILES("participants without SMILES descriptor"),
    TOO_MANY_PARTICIPANT("too many participants"),
    INCOHERENT_RXNSMILES("RxnSMILES do not match reactions substrates and products");

    private final String message;

    private Flag(String value) {
        message = value;
    }

    public String message() {
        return message;
    }

}

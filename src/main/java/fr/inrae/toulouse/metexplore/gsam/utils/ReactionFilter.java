package fr.inrae.toulouse.metexplore.gsam.utils;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.regex.Pattern;

public class ReactionFilter {

    int maxNbOfParticipant = Integer.MAX_VALUE;
    int maxNbOfAtoms = Integer.MAX_VALUE;
    boolean onlyBalanced = false;
    //the regex used to check formula validity. Reaction is kept if the pattern match at least one reactant formula and one product formula
    Pattern smileRegex;

    //flags that will mark for filtering
    EnumSet<Flag> nonEligibleFlags = EnumSet.of(Flag.TOO_MANY_ATOMS,Flag.TOO_MANY_PARTICIPANT);

    public ReactionFilter(int maxNbOfParticipant,int maxNbOfAtoms, boolean onlyBalanced, Pattern smileRegex){
        this.maxNbOfParticipant=maxNbOfParticipant;
        this.maxNbOfAtoms=maxNbOfAtoms;
        this.smileRegex=smileRegex;
        this.onlyBalanced=onlyBalanced;

        if(smileRegex!=null) nonEligibleFlags.add(Flag.DO_NOT_CONTAINS_ATOM_TYPE);
        if(onlyBalanced) nonEligibleFlags.add(Flag.NON_BALANCED);
    }

    /**
     * decide if an checked reaction should be kept according to its flag and defined eligibility criterion.
     * An unchecked reaction will always pass.
     * @param r
     * @return
     */
    public boolean keep(GSAMReaction r) {
        if (!Collections.disjoint(r.getFlags(), nonEligibleFlags)) return false;
        return true;
    }

    /**
     * check on the fly if a reaction should be kept, do not set flags
     * @param r
     * @return
     */
    public boolean keepUnchecked(GSAMReaction r){
        if(!Collections.disjoint(r.getFlags(), nonEligibleFlags)) return false;

        //Reactions with too many participants can cause severe performance issues while commonly being of low relevance.
        //An example is the "biomass production" reaction present in most SBML model:
        //relevant in the context of constraint based modelling, but not for atom tracking.
        if(!hasValidNumberOfParticipant(r, maxNbOfParticipant)) return false;
        int c1 = getNumberOfAtoms(r.getSubstrates());
        int c2 = getNumberOfAtoms(r.getProducts());
        if(c1+c2>=maxNbOfAtoms) return false;
        //example: for building the carbon skeleton graph, check that at least one substrate and one product contain carbon.
        if(smileRegex!=null){
            if(!hasAtLeastOneValidSmile(r.getProducts(),smileRegex))  return false;
            if(!hasAtLeastOneValidSmile(r.getSubstrates(),smileRegex))  return false;
        }
        if(onlyBalanced){
            //same overall number of atoms doesn't mean it's balanced
            //but different ones surely means it isn't.
            if(c1!=c2) return false;
        }

        return true;
    }

    /**
     * Check values of several properties of the reactions and set flags if not in range, according to the expected values defined in the filter build.
     * @param r a reaction
     */
    public void checkAndFlag(GSAMReaction r){
        //Reactions with too many participants can cause severe performance issues while commonly being of low relevance.
        //An example is the "biomass production" reaction present in most SBML model:
        //relevant in the context of constraint based modelling, but not for atom tracking.
        if(!hasValidNumberOfParticipant(r, maxNbOfParticipant)) r.flag(Flag.TOO_MANY_PARTICIPANT);
        int c1 = getNumberOfAtoms(r.getSubstrates());
        int c2 = getNumberOfAtoms(r.getProducts());
        if(c1+c2>=maxNbOfAtoms) r.flag(Flag.TOO_MANY_ATOMS);
        //example: for building the carbon skeleton graph, check that at least one substrate and one product contain carbon.
        if(smileRegex!=null){
            if(!hasAtLeastOneValidSmile(r.getProducts(),smileRegex)) {
                r.flag(Flag.DO_NOT_CONTAINS_ATOM_TYPE);
            }else if(!hasAtLeastOneValidSmile(r.getSubstrates(),smileRegex)){
                r.flag(Flag.DO_NOT_CONTAINS_ATOM_TYPE);
            }
        }
        //same overall number of atoms doesn't mean it's balanced
        //but different ones surely means it isn't.
        if(c1!=c2) r.flag(Flag.NON_BALANCED);
    }


    public boolean hasValidNumberOfParticipant(GSAMReaction r, int n){
        return (r.getSubstrates().size()+r.getProducts().size() <= n);
    }

    public int getNumberOfAtoms(Collection<GSAMCompound> compounds){
        int count=0;
        for(GSAMCompound c : compounds){
            count+=c.getCDKmolecule().getAtomCount();
        }
        return count;
    }

    public boolean hasAtLeastOneValidSmile(Collection<GSAMCompound> compounds, Pattern smileRegex){
        for(GSAMCompound c : compounds){
            if(smileRegex.matcher(c.getSmile()).matches()) return true;
        }
        return false;
    }

    public EnumSet<Flag> getNonEligibleFlags() {
        return nonEligibleFlags;
    }

    public void setNonEligibleFlags(EnumSet<Flag> nonEligibleFlags) {
        this.nonEligibleFlags = nonEligibleFlags;
    }

}

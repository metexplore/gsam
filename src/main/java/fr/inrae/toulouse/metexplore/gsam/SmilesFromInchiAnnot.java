package fr.inrae.toulouse.metexplore.gsam;

import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.sbml.jsbml.*;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmilesFromInchiAnnot {

    private static final Logger logger = LogManager.getLogger(SmilesFromInchiAnnot.class);

    //Our data of interest, molecular structure, can be extracted from InChIs.
    //InChIs are molecular descriptor
    //As part of the miriam initiative, they can be provided as annotations, and identified through their specific namespace (miriam uri).
    public static Pattern uriPattern = Pattern.compile("^https?://identifiers\\.org/inchi/(.+)$",Pattern.CASE_INSENSITIVE);
    public static Pattern inchiPattern = Pattern.compile("^InChI\\=1S?\\/[A-Za-z0-9\\.]+(\\+[0-9]+)?(\\/[cnpqbtmsih][A-Za-z0-9\\-\\+\\(\\)\\,\\/\\?\\;\\.]+)*$");

    //This class act as a wrapper of CDK classes and will be handling atom container internally
    private SmilesGenerator generator;
    private InChIGeneratorFactory inchiFactory;

    public SmilesFromInchiAnnot(){
        generator = new SmilesGenerator(SmiFlavor.UniversalSmiles);
        try {
            inchiFactory = InChIGeneratorFactory.getInstance();
        } catch (CDKException e) {
            logger.trace("",e);
        }
    }

    public static boolean isValidInChi(String inchi){
        return inchiPattern.matcher(inchi).matches();
    }

    private String convertInchiToSmile(String inchi) throws CDKException{
        InChIToStructure struct = inchiFactory.getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());
        IAtomContainer m = struct.getAtomContainer();

        return createSmiles(m);
    }

    private String createSmiles(IAtomContainer m) throws CDKException{
        String smile = generator.create(m);
        return smile;
    }

    // Extract inchi from Jsbml specie object build from SBML parsing
    // return "?" by default. If multiple inchi are present, take the last valid one.
    public String getValidInchiFromAnnot(Species s){
        String inchi = "?";
        //get entry's annotations
        Annotation annot = s.getAnnotation();
        //keep only descriptor annotations
        for(CVTerm term : annot.filterCVTerms(CVTerm.Qualifier.BQB_IS)){

            //keep descriptors identified as InChIs
            List<String> resources = term.filterResources(uriPattern);
            for(String resource : resources){
                Matcher uriMatcher = uriPattern.matcher(resource);
                //check if InChI is valid
                if(uriMatcher.matches()){
                    String id = uriMatcher.group(1);
                    if(SmilesFromInchiAnnot.isValidInChi(id)){
                        //extract InChI string
                        inchi = id;
                    }
                }
            }
        }
        return inchi;
    }

    // Extract inchi from Jsbml specie object build from SBML parsing
    // return "?" by default. Doesn't check for Inchi validity.
    // If multiple inchi are referenced, return the first encountered one
    public String getInchiFromAnnot(Species s){
        //get entry's annotations
        Annotation annot = s.getAnnotation();
        //keep only descriptor annotations
        for(CVTerm term : annot.filterCVTerms(CVTerm.Qualifier.BQB_IS)){

            //keep descriptors identified as InChIs
            List<String> resources = term.filterResources(uriPattern);
            for(String resource : resources){
                Matcher uriMatcher = uriPattern.matcher(resource);
                //check if InChI is valid
                if(uriMatcher.matches()){
                    String id = uriMatcher.group(1);
                    return id;
                }
            }
        }
        return "?";
    }

    public static void main(String[] args) throws IOException, XMLStreamException {

        SmilesFromInchiAnnot convertor = new SmilesFromInchiAnnot();

        //parse SBML using JSBML library
        //------------------------------
        SBMLReader reader = new SBMLReader();
        SBMLDocument doc = reader.readSBMLFromFile(args[0]);
        Model model = doc.getModel(); //JSBML model stores all data from SBML file

        //Process information related to compounds
        //----------------------------------------
        //Our application requires information about compounds' mass, atomic composition, and chemical structure.
        //All this information will be stored in our Compound class.
        HashMap<String, GSAMCompound> entries = new HashMap<>();


        //Extract inchi for each specie in the JSBML model.
        ListOf<Species> species =  model.getListOfSpecies();
        int n = 0;
        int m = 0;
        for(Species s : species){

            String smile="*";

            //Parse annotation to extract InChI if provided with standard Miriam uri
            String inchi = convertor.getInchiFromAnnot(s);

            if(!inchi.equals("?")){
                n++;

                if(isValidInChi(inchi)){

                    //convert inchi to smile
                    try {
                        smile = convertor.convertInchiToSmile(inchi);
                        m++;
                    } catch (CDKException e) {
                        logger.error("[INCHI] InChI of entry "+s.getId()+" can't be converted");
                        logger.debug("[INCHI] "+s.getId()+": "+inchi);
                        logger.trace("",e);
                    }

                }else{
                    logger.error("[INCHI] InChI of entry "+s.getId()+" isn't valid");
                    logger.debug("[INCHI] "+s.getId()+": "+inchi);
                }

            }else{
                logger.warn("[INCHI] no InChI annotation found for entry "+s.getId());
            }

            //Create the compound object.
            //The compound object store the sbml entry and add some extra information such as mass and smiles descriptor
            //The extra information are computed from the InChI descriptor that represent the compound structure
            //The mass will be used to compute transitions' mass shifts for each reaction
            //The smile will be used to compute the atom mapping between substrates and products of reactions
            GSAMCompound cpd = new GSAMCompound(s);
            try {
                cpd = new GSAMCompound(s, smile);
            } catch (CDKException e) {
                logger.error("[SMILES] Smiles of entry "+s.getId()+" isn't valid");
                logger.debug("[INCHI] "+s.getId()+": "+inchi);
                logger.debug("[SMILES] "+s.getId()+": "+smile);
            }

            entries.put(s.getId(), cpd);

        }

        logger.info(model.getListOfSpecies().size()+" compounds imported");
        logger.info(n+" compounds with InChI");
        logger.info(entries.size()+" compounds exported");
        logger.info(m+" compounds with SMILE");

        //Export results
        //TODO: option to skip compound without smile
        //TODO: option to print mass :Isomet:
        BufferedWriter w;
        try {
            w = new BufferedWriter(new FileWriter(args[1]));
            for(GSAMCompound c : entries.values()){
                w.write(c.getSbmlSpecie().getId()+"\t"+c.getSmile());
                w.newLine();
            }
            w.close();
        } catch (IOException e) {
            logger.fatal("[I/O] error while writing SMILES file");
            logger.trace("",e);
        }
    }

}

package fr.inrae.toulouse.metexplore.gsam;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;
import fr.inrae.toulouse.metexplore.gsam.AAM.AAMSmileParser;
import fr.inrae.toulouse.metexplore.gsam.AAM.AAMSmilesComputor;
import fr.inrae.toulouse.metexplore.gsam.AAM.AtomMappingContainer;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMCompound;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMReaction;
import fr.inrae.toulouse.metexplore.gsam.data.GSAMTransition;
import fr.inrae.toulouse.metexplore.gsam.utils.*;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Species;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public class ComputeGSAM {

	//PARAMS
	//----------------
	@Option(name = "-i", aliases = {"--sbml"}, usage = "input SBML file", required = true)
	String sbmlPath = null;
	@Option(name = "-s", aliases = {"--smi"}, usage = "input SMILES table", required = true)
	String smilesPath = null;
	@Option(name = "-o", aliases = {"--outdir"}, usage = "path to the output directory", required = true)
	String outputDir = null;

	//OPTIONS
	//----------------
	@Option(name = "-a", aliases = {"--atom"},usage = "filter option: atom tracked", required = false)
	String atomTracked = "C";
	@Option(name = "-m", aliases = {"--max"},usage = "filter option: max number of atoms in reaction", required = false)
	int maxAtoms = 1000;
	@Option(name = "-c", aliases = {"--count"},usage = "output option: write number of mapped atoms rather than whole map", required = false)
	boolean atomCountOnly = false;
	@Option(name = "-influx", usage = "output option: export expanded-FTBL file suitable for influx software", required = false)
	boolean ftbl = false;

	@Option(name = "-sc", aliases = {"--sidecompounds"}, usage = "A single column file containing side compounds' identifiers (one per line). Side compounds will be ignored during export (ftbl and transitions file)", required = false)
	String side;

	@Option(name = "-col1", aliases = {"--idcolumn"},usage = "SMILES import option: column containing GSMN compounds ids", required = false)
	int colid = 1;
	@Option(name = "-col2", aliases = {"--smicolumn"},usage = "SMILES import option: column containing SMILES", required = false)
	int colsmi = 2;
	@Option(name = "-sh", aliases = {"--skpiheader"},usage = "SMILES import option: table contains header", required = false)
	boolean skipHeader = false;
	@Option(name = "-sep", aliases = {"--separator"},usage = "SMILES import option: character delimiting columns", required = false)
	String sep = "\t";

	//HELP
	//----------------
	@Option(name = "-h", aliases = {"--help"},usage = "prints the help", required = false)
	private Boolean h = false;
	@Option(name = "-v", aliases = {"--version"},usage = "prints the version", required = false)
	private Boolean v = false;

	public static void main(String[] args) throws IOException {
		ComputeGSAM cli = new ComputeGSAM();
		cli.parseArguments(args);
		cli.run();
	}

	public void run(){
		//Set log
		LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		FileAppender appender = new FileAppender();
		appender.setContext(loggerContext);
		appender.setFile(outputDir+"/gsam.log");
		PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		encoder.setContext(loggerContext);
		encoder.setPattern("%d{yyy-MM-dd HH:mm} %logger{0}, %-5level - %msg%n");
		encoder.start();
		appender.setEncoder(encoder);
		appender.start();
		Logger logger = loggerContext.getLogger(ComputeGSAM.class.getPackageName());
		logger.addAppender(appender);


		//start GSAM computation
		logger.info("[VERSION] GSAM: "+getVersion());
		logger.info("[INPUT] SBML: "+sbmlPath);
		logger.info("[INPUT] SMILES: "+smilesPath);
		logger.info("[PARAM] Atom tracked: "+atomTracked);
		logger.info("[PARAM] Max total number of atoms in reaction: "+maxAtoms);
		logger.info("[PARAM] "+sbmlPath);
		Instant start = Instant.now();

		//parse SBML using JSBML library
		//------------------------------
		Model model = null;
		try{
			model= ParseSBML.parseSbml(sbmlPath);
		} catch (XMLStreamException e) {
			logger.error("[SBML] error in SBML file");
			logger.trace("",e);
			System.exit(1);
		} catch (IOException e) {
			logger.error("[I/O] error while reading SBML file");
			logger.trace("",e);
			System.exit(1);
		}

		//Create compounds
		//----------------------------------------
		//Import SMILES
		//========================================
		//Our data of interest are the topology of the biochemical network, which can be extracted from sbml model,
		//and chemical structures of its component, that can be represented has SMILES.
		//SMILES are molecular descriptor that store atom composition and connectivity.
		//Such information is not covered by SMBL standard and is provided in a separated file
		BufferedReader in;
		HashMap<String, String> SMILEmap = new HashMap<>();
		int n=0;
		try {
			in = new BufferedReader(new FileReader(smilesPath));
			String l = in.readLine();
			colid=colid-1;
			colsmi=colsmi-1;
			if(skipHeader) l = in.readLine();
			while(l!=null){
				n++;
				String[] parsedL = l.split(sep);
				//If there is more than one SMILES for a compound, keep the shortest
				if(!SMILEmap.containsKey(parsedL[colid]) || SMILEmap.get(parsedL[colid]).length()>parsedL[colsmi].length()){
					SMILEmap.put(parsedL[colid],parsedL[colsmi]);
				}
				l = in.readLine();
			}
			in.close();
		} catch (IOException e) {
			logger.error("[I/O] error while reading SMILES file");
			logger.trace("",e);
			System.exit(0);
		}

		logger.info(n+" SMILES statement parsed");
		logger.info(SMILEmap.size()+" compound-SMILES associations found");

		//build compound list from parsed SBML and imported SMILES
		//================================================================================
		HashMap<String, GSAMCompound> entries = ParseSBML.createCompounds(model,SMILEmap);


		//Create reactions
		//----------------------------------------
		//build reaction list from parsed SBML and created compounds
		//================================================================================
		HashMap<String, GSAMReaction> reactions = ParseSBML.createReactions(model, entries);

		//filter reactions
		//================================================================================
		List<GSAMReaction> validReactions = new ArrayList<>();
		ReactionFilter f = new FilterBuilder()
				.maxCountOfTotalAtoms(maxAtoms)
				.mustContainsAtoms(atomTracked)
				.build();
		for(GSAMReaction reaction : reactions.values()){
			f.checkAndFlag(reaction);
			if(!reaction.getFlags().isEmpty()){
				for(Flag flag : reaction.getFlags()){
					logger.debug("[RXN] "+reaction.getSbmlReaction().getId()+": "+flag.toString());
				}
				logger.debug("[RXN] "+reaction.getSbmlReaction().getId()+": "+ reaction.getEquation());
			}

			if(f.keep(reaction)) {
				validReactions.add(reaction);
			}else {
				logger.warn("[RXN] "+reaction.getSbmlReaction().getId()+": not selected for AAM");
			}
		}
		logger.info(model.getListOfReactions().size()+" reactions in SBML");
		logger.info(reactions.size()+" reactions successfully parsed");
		logger.info(validReactions.size()+" reactions selected for AAM computation");

		//group reaction by rxn smiles to avoid redundant computation (same AAM)
		//================================================================================
		Map<String, List<GSAMReaction>> rxnSmilesMap = validReactions.stream().collect(Collectors.groupingBy(r -> r.getRxnSmiles()));
		logger.info(rxnSmilesMap.size()+" non-redundant RxnSMILES");

		//compute atom mapping for reaction groups
		//================================================================================
		//TODO:import compted rxnSmiles
		AtomicInteger j= new AtomicInteger();
		AtomicInteger l= new AtomicInteger();
		l.getAndAdd(1);
		int ttl = validReactions.size();
		AAMSmilesComputor aamBuilder = new AAMSmilesComputor();
		rxnSmilesMap.entrySet().stream().forEach((entry) -> {
			List<GSAMReaction> reactionSet = entry.getValue();
			long t0 = System.currentTimeMillis();
			AAMSmileParser aamParser = new AAMSmileParser();
			aamParser.setAtoms(atomTracked);

			logger.debug(l + "/" + ttl +" "+reactionSet.get(0).getSbmlReaction().getId());
			String RxnSmiles = "?";

			try {
				RxnSmiles = aamBuilder.compute(reactionSet.get(0));
			} catch (Exception e) {
				for(GSAMReaction r : reactionSet){
					logger.error("[AAM] "+r.getSbmlReaction().getId()+": error while computing atom mapping");
					logger.debug("[AAM] "+r.getSbmlReaction().getId()+": "+ r.getEquation());
					logger.debug("[AAM] "+r.getSbmlReaction().getId()+": "+r.getRxnSmiles());
				}
				logger.trace("",e);
			}

			String finalRxnSmiles = RxnSmiles;
			reactionSet.stream().forEach(r -> r.setRxnSmiles(finalRxnSmiles));

			if (!RxnSmiles.equals("?")) {
				reactionSet.stream().forEach(r -> {
					try {
						AtomMappingContainer aam = new AtomMappingContainer();
						aamParser.parseMapping(r.getSubstrates(), r.getProducts(), finalRxnSmiles, aam);
						r.setAtomMapping(aam);
					} catch (Exception e) {
						logger.error("[AAM] "+ r.getSbmlReaction().getId()+": error while computing atom mapping");
						logger.debug("[AAM] "+r.getSbmlReaction().getId()+": "+ r.getEquation());
						logger.debug("[AAM] "+r.getSbmlReaction().getId()+": "+r.getRxnSmiles());
						logger.trace("",e);
					}
				});
				j.getAndAdd(reactionSet.size());
			}
			l.getAndAdd(reactionSet.size());
//			long t1 = System.currentTimeMillis();
//			float msec = t1 - t0;
//			float sec= msec/1000F;
//			float minutes=sec/60F;
		});
		for(Map.Entry<String,Set<String>> e : aamBuilder.getSMILESmap().entrySet()){
			if(e.getValue().size()>1){
				logger.error("[AAM] compound "+e.getKey()+" has non canonic SMILES ("+e.getValue().size()+")");
				logger.debug("[AAM] "+e.getKey()+": "+e.getValue());
			}
		}
		logger.info(j+" reactions with AAM");

		//Export compound table
		//================================================================================
		BufferedWriter w;
		try {
			w = new BufferedWriter(new FileWriter(outputDir+"/reactions.tab"));
			for(GSAMReaction r : validReactions){ //TODO: option to export only reaction with AAM?
				w.write(r.getSbmlReaction().getId()
						+"\t"+r.getSubstrates().stream().map(GSAMCompound::getSbmlSpecie).map(Species::getId).collect(Collectors.joining(","))
						+"\t"+r.getProducts().stream().map(GSAMCompound::getSbmlSpecie).map(Species::getId).collect(Collectors.joining(","))
						+"\t"+r.getRxnSmiles());
				w.newLine();
			}
			w.close();
		} catch (IOException e) {
			logger.error("[I/O] error while writing reaction file");
			logger.trace("",e);
		}


		//Build transition and process atom mapping
		//----------------------------------------
		HashMap<String,Collection<GSAMTransition>> indexedTransitions = ParseSBML.createTransitions(reactions);
		Collection<GSAMTransition> transitions =
				indexedTransitions.values().stream().collect(HashSet::new,Set::addAll,Set::addAll);
		logger.info(transitions.size()+" transitions extracted from "+indexedTransitions.size()+" reactions");


		//Import Side Compounds
		//================================================================================
		HashSet<String> sideCompounds = new HashSet<>();
		if(side!=null){
			try {
				in = new BufferedReader(new FileReader(side));
				String line = in.readLine();
				while(line!=null){
					line = line.trim();
					if(entries.keySet().contains(line)){
						sideCompounds.add(line);
					}else{
						logger.warn("[SIDE] Side compound not found: "+line);
					}
					line = in.readLine();
				}
				in.close();
			} catch (IOException e) {
				logger.error("[I/O] error while reading Side compound file");
				logger.trace("",e);
				System.exit(0);
			}

			logger.info(n+" Side compounds file parsed");
			logger.info(SMILEmap.size()+" Side compounds found");
		}

		//Export FTBL format
		//================================================================================
		if(ftbl) {
			try {
				w = new BufferedWriter(new FileWriter(outputDir + "/reactions.ftbl"));
				FTBLExporter exporter = new FTBLExporter(validReactions,indexedTransitions);
				if(!sideCompounds.isEmpty()) exporter.setSideCompoundsToIgnore(sideCompounds);
				exporter.exportFTBL(w);
				w.close();
			} catch (IOException e) {
				logger.error("[I/O] error while writing ftbl file");
				logger.trace("", e);
			}
		}

		//Export transitions table
		//================================================================================
		try {
			w = new BufferedWriter(new FileWriter(outputDir+"/transitions.tab"));

			for(GSAMTransition t : transitions){

				String sourceAtoms = atomCountOnly ? String.valueOf(t.getSourceAtomLabels().stream().collect(HashSet::new,Set::addAll,Set::addAll).size()) : t.getSourceAtomLabels().stream().map(
						sublist -> sublist.stream()
								.collect(Collectors.joining("-")))
						.collect(Collectors.joining(" . "));

				String targetAtoms = atomCountOnly ? String.valueOf(t.getTargetAtomLabels().stream().collect(HashSet::new,Set::addAll,Set::addAll).size()) : t.getTargetAtomLabels().stream().map(
						sublist -> sublist.stream()
								.collect(Collectors.joining("-")))
						.collect(Collectors.joining(" . "));

				String conserved = atomCountOnly ? String.valueOf(t.getConservedAtomLabels().size()) : String.join(",",t.getConservedAtomLabels());

				w.write(t.getSource().getSbmlSpecie().getId()
						+"\t"+t.getSource().getMass()
						+"\t"+sourceAtoms

						+"\t"+t.getTarget().getSbmlSpecie().getId()
						+"\t"+t.getTarget().getMass()
						+"\t"+targetAtoms

						+"\t"+t.getReaction().getSbmlReaction().getId()
						+"\t"+t.getMassshift()
						+"\t"+conserved
				);
				w.newLine();
			}
			w.close();
		} catch (IOException e) {
			logger.error("[I/O] error while writing transition file");
			logger.trace("",e);
		}

		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toSeconds();
		logger.info("time elapsed: "+timeElapsed+"s");

	}


	public static String getLabel() {return "\n" +
			"   ______ _____   ___      __  ___ \n" +
			"  / ____// ___/  /   |    /  |/  / \n" +
			" / / __  \\__ \\  / /| |   / /|_/ /  \n" +
			"/ /_/ / ___/ / / ___ |_ / /  / /   \n" +
			"\\____(_)____(_)_/  |_(_)_/  /_(_)  \n" +
			"                                  \n";}

	public static String getDescription() {return "" +
			"A tool that perform Genome Scale Atom Mapping. The results can be exploited to build carbon skeleton networks for topological analysis of genome-scale metabolic metabolic networks (GSMN), and analyse atom tracking results.\n" +
			"GSAM is a built around the Reaction Decoder Toolkit, a library which compute atom mapping from reaction represented as RxnSmiles.\n" +
			"It use the JSBML library to import a GSMN in standard format, automatically generate all the RxnSmiles from provided compound structure, launch the atom mapping and parse the results to render meaningful substrate-products transitions.\n" +
			"Since many reactions in SBML are not suited for AAM (such as biomass production), GSAM provides flexible filtering utilities and extensive logs for adapting GSMN to AAM.";
	}

	public static String getUsage() {return "" +
			"\nGSAM requires a metabolic network in SBML (Systems Biology Markup Language) file format and a table containing SMILES (Simplified molecular-input line-entry system) representing the chemical structure of the compounds in the model\n" +
			"Find more about the network format at http://sbml.org and the structure representation at https://www.daylight.com/smiles/index.html\n" +
			"SBML networks can be retrieved, for example, from MetExplore (http://www.metexplore.fr/)." +
			"\n\nUsage:\n" +
			"\n\t```" +
			"\n\tjava -jar GSAM.jar -i path/to/sbml.xml -s path/to/smiles.tab -o path/to/outputDir" +
			"\n\t```\n";
	}

	public String getVersion(){
		return this.getClass().getPackage().getImplementationVersion();
	}

	public void printVersion(){
		System.out.println(this.getVersion());
	}

	public void printHeader()
	{
		System.out.println(this.getLabel());
		System.out.println("version "+this.getVersion()+"\n");
		System.out.println(this.getDescription());
		System.out.println(this.getUsage());
	}

	protected void parseArguments(String[] args) {
		CmdLineParser parser = new CmdLineParser(this);

		try {
			parser.parseArgument(args);
			if(this.h){
				this.printHeader();
				parser.printUsage(System.out);
				System.exit(0);
			}else if(this.v){
				this.printVersion();
				System.exit(0);
			}
		} catch (CmdLineException e) {
			if(this.h){
				this.printHeader();
				parser.printUsage(System.out);
				System.exit(0);
			}
			else if(this.v){
				this.printVersion();
				System.exit(0);
			}
			else{
				System.err.println("Error in arguments\n");
				System.err.println(this.getUsage());
				parser.printUsage(System.err);
				System.exit(0);
			}
		}
	}



}
